$(document).ready(function(){
	
	//Table tariffs
	$('#compare').click(function(){
		$('.table-price').css('display')=='none' ? 
			$('.table-price').slideDown('fast', function(){
				$('html, body').animate({ scrollTop: $('.table-price').offset().top - 150 }, 600)
			}) : 
			$('.table-price').slideUp('fast', function(){
				$('html, body').animate({ scrollTop: $('.table-price').offset().top - $('.select-your').height() }, 800)
			});
	});

	//Call
	$('#call').click(function(e){
		$(this).addClass('active');
		$('.selectbox-call ul').addClass('open');
		$('.phone').focus();
		e.stopPropagation();
	});
	$(document).bind('keyup', function(e){
		if(e.keyCode==27) {
			$('#call').removeClass('active');
			$('.selectbox-call ul').removeClass('open');
		}
	});

	//Select
	$('.input').click(function(e){
		$(this).toggleClass('active');
		$(this).parent().find('.selectbox').slideToggle(100);
		e.stopPropagation();
	});
	$(document).click(function(){
		$('.input').removeClass('active');
		$('.selectbox').slideUp(100);
	})

	//Phone
	$('#sendCallback').click(function(){
		if ($('.phone').val() !='') {
			$('.send-request').hide();
			$('.request').show();
		} else {
			$('.phone').addClass('error').focus();
			setTimeout(function(){
		    	$('.phone').removeClass('error')
		    },1000);
		};
	});


	/* Parallax */
	if($('#main-page').length > 0)
	{
		$(window).scroll(function(){
			$('#player').css({ 'background-position': 'center ' + ($(document).scrollTop() - $('#player').offset().top) * 0.5 + 'px' });
			$('.awards').css({ 'background-position': 'center ' + ($(document).scrollTop() - $('.awards').offset().top) * 0.5 + 'px' });
			$('.excerpt').css({ 'background-position': 'center ' + ($(document).scrollTop() - $('.excerpt').offset().top) * 0.5 + 'px' });
		});
	}

	//Zoom Pic Blog
	$('section .pic img').load(function(){
		changePic();
	});
	function changePic(){
		$('section .pic img').each(function(){
			var top = ($(this).height() - $(this).parent('section .pic').height())/2;
			$(this).css({
				'top': - top + 'px'
			})
		});
	};
	changePic();

	//Answer
	$('.answer li').click(function(){
		var active = ($(this).hasClass('active')) ? true : false;
		$('.answer li').removeClass('active');
		$('.answer li p').slideUp('fast');
		if(!active)
		{
			$(this).children('.answer li p').slideDown('fast');
			$(this).addClass('active');
		} else {
			$(this).children('.answer li p').slideUp('fast');
			$(this).removeClass('active');
		}
	});

	//The number of sets
	$('.list-item .minus').css('visibility','hidden');
	$('.minus').click(function(){
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 0 ? 0 : count;
        if (count == 0) {
        	$(this).css('visibility','hidden');
        }
        $input.val(count);
        $input.change();
    });
    $('.plus').click(function(){
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        $(this).parent().find('.minus').css('visibility','visible');
    });


	//Resize
	$(window).resize(function(){
		$('.preview .wrap').css({
			'left': ($(document).width() - $('.preview .wrap').width())/2 + 'px'
		});
		$('.awards .wrap').css({
			'top': ($('.awards').height() - $('.awards .wrap').outerHeight())/2 - 110 + 'px'
		});
		$('.excerpt h3').css({
			'position':'relative',
			'top': ($('.excerpt').height() - $('.excerpt h3').height())/2 + 'px'
		});
	});
	$(window).resize();

});