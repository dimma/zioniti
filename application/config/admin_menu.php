<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'prices' => array(
		'url'	=> 'prices',
		'icon'	=> 'usd',
		'name'	=> 'Цены',
	),
	'blog' => array(
		'url'	=> 'blog',
		'icon'	=> 'edit',
		'name'	=> 'Блог',
	),
	'requests' => array(
		'url'	=> 'requests',
		'icon'	=> 'envelope',
		'name'	=> 'Заявки',
		'items'	=> array(
			'callback' => array(
				'url'	=> 'callback',
				'icon'	=> 'phone',
				'name'	=> 'Обратный звонок',
			),
			'order_lite' => array(
				'url'	=> 'lite',
				'icon'	=> 'th-large',
				'name'	=> 'Zioniti Lite',
			),
			'order_pro' => array(
				'url'	=> 'pro',
				'icon'	=> 'th-list',
				'name'	=> 'Zioniti PRO',
			),
			'order_server' => array(
				'url'	=> 'server',
				'icon'	=> 'th',
				'name'	=> 'Zioniti Server',
			),
			'archive' => array(
				'url'	=> 'archive',
				'icon'	=> 'archive',
				'name'	=> 'Архив',
			),
		),
	)
);
?>