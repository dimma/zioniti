<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'products' => array(
		'url' => 'products',
		'class' => 'products',
		'name' => 'Продукты',
		'title' => 'Продукты',
	),
	'business' => array(
		'url' => 'business',
		'class' => 'business',
		'name' => 'Бизнес-решения',
		'title' => 'Бизнес-решения',
	),
	'blog' => array(
		'url' => 'blog',
		'class' => 'blog',
		'name' => 'Блог',
		'title' => 'Блог',
	),
);
?>