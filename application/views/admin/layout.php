<!DOCTYPE html>
<html>
	<head>
		<title><?=$window_title?></title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta content='text/html;charset=utf-8' http-equiv='content-type'>
		<meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.' name='description'>

		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>

		<!--[if lt IE 9]>
		<script src="/assets/admin/javascripts/ie/html5shiv.js" type="text/javascript"></script>
		<script src="/assets/admin/javascripts/ie/respond.min.js" type="text/javascript"></script>
		<![endif]-->
	</head>
	<body class="contrast-sea-blue">
		<header>
			<nav class='navbar navbar-default'>
			<a class='navbar-brand' href="/">
				<? /* <img width="138" height="21" class="logo" alt="Flatty" src="/assets/i/logo_top.png" /> */ ?>
				Zioniti Admin
			</a>
			<a class='toggle-nav btn pull-left' href='javascript:void(0);'><i class='icon-reorder'></i></a>
			<ul class='nav'>
				<? /*
				<li class='dropdown light only-icon'>
					<a class='dropdown-toggle' data-toggle='dropdown' href='#'>
						<i class='icon-cog'></i>
					</a>
					<ul class='dropdown-menu color-settings'>
						<li>Всякие настройки</li>
					</ul>
				</li>
				*/ ?>
				<li class='dropdown dark user-menu'>
					<a class='dropdown-toggle' data-toggle='dropdown' href='#'>
						<? /* <img width="23" height="23" alt="<?=$user->name?>" src="/assets/images/avatar.jpg" /> */ ?>
						<span class='user-name'><?=$user_title?></span>
						<b class='caret'></b>
					</a>
					<ul class='dropdown-menu'>
						<li>
							<a href="javascript:void(0);">
								<i class='icon-user'></i>
								Профиль
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<i class='icon-cog'></i>
								Настройки
							</a>
						</li>
						<li class='divider'></li>
						<li>
							<a href='/admin/logout'>
								<i class='icon-signout'></i>
								Выход
							</a>
						</li>
					</ul>
				</li>
			</ul>
			</nav>
		</header>
    	<div id='wrapper'>
        	<div id='main-nav-bg'></div>
            <nav id='main-nav'>
                <div class='navigation'>
					<? /*
                    <div class='search'>
                        <form action='search_results.html' method='get'>
                            <div class='search-wrapper'>
                                <input value="" class="search-query form-control" placeholder="Search..." autocomplete="off" name="q" type="text" />
                                <button class='btn btn-link icon-search' name='button' type='submit'></button>
                            </div>
                        </form>
                    </div>
 					*/ ?>
                    <ul class='nav nav-stacked'>
                        <? foreach($menu as $k => $v) : ?>
	                        <? if($user->has('roles', ORM::factory('role', array('name' => $v['url'])))) : ?>
		                        <li<? if ($controller == $v['url']) : ?> class="active"<? endif; ?>>
						<a href="/admin/<?=$v['url']?>"><i class='icon-<?=$v['icon']?>'></i><span><?=$v['name']?></span></a>
						<? if (isset($v['items'])) : ?>
							<ul class='in nav nav-stacked'>
								<? foreach($v['items'] as $sub) : ?>
									<li<? if($controller == $v['url'] && $action == $sub['url']) : ?> class="active"<? endif; ?>>
										<a href="/admin/<?=$v['url']?>/<?=$sub['url']?>">
											<i class='icon-<?=$sub['icon']?>'></i><span><?=$sub['name']?></span>
											<? if($v['url'] == 'requests'  && $sub['url'] == 'new' && isset($digits['requests'])) : ?>
												<span class="pull-right">
													<? if($digits['requests']) : ?>
														<span class="label label-danger" id="requests-count"><?=$digits['requests']?></span>
													<? endif; ?>
												</span>
											<? endif; ?>
										</a>
									</li>
								<? endforeach; ?>
							</ul>
						<? endif; ?>
		                        </li>
				<? endif; ?>
                        <? endforeach; ?>
                     </ul>
                </div>
            </nav>
            <section id='content'>
                <div class='container'>
                    <div class='row' id='content-wrapper'>
                        <div class='col-xs-12'>
                            <div class='row'>
                                <div class='col-sm-12'>
                                    <div class='page-header'>
                                        <h1 class='pull-left'><i class='icon-<?=$icon?>'></i>&nbsp;<span><?=$title?></span></h1>
										<? /* Контент в хедере */ ?>
										<?=$title_content?>
                                    </div>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-sm-12'><?=$content?></div>
                            </div>
                        </div>
                    </div>

                    <footer id='footer'>
                        <div class='footer-wrapper'>
                            <div class='row'>
                                <div class='col-sm-6 text'>
                                    Copyright &copy; <?=date('Y')?> Site Expo
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </section>
        </div>
    <!-- javascript
    ================================================== -->
    <? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
    </body>
</html>
