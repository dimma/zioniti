<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<div class='tabbable'>
					<div class='tab-content'>
						<div class='tab-pane active' id='tab1'>
							<table class="table table-hover">
								<tbody>
									<tr class="warning">
										<th class="thin-col">&nbsp;</th>
										<th>Наименование</th>
										<th class="thin-col">Стоимость ($)</th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list base-services" type="1" package="base">
									<? foreach($prices as $k => $v) : ?>
										<tr price_id="<?=$v->id?>" id="price_<?=$v->id?>">
											<td class="thin-col"><?=$v->id?></td>
											<td><input type="text" name="price[<?=$v->id?>][title]" value='<?=$v->title?>' class="form-control"/></td>
											<td class="thin-col"><input type="text" name="price[<?=$v->id?>][price]" value='<?=$v->price?>' class="form-control text-right s-price"/></td>
										</tr>
									<? endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>