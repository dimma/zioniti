<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<? if ($requests->count()) : ?>
					<table class="table table-hover requests-table">
						<thead>
							<tr>
								<th class="thin-col text-center">ID</th>
								<th>Телефон</th>
								<th class="thin-col">Дата</th>
								<th class="thin-col"></th>
							</tr>
						</thead>
						<tbody id="requests-list">
						<? foreach($requests as $request) : ?>
							<tr request_id="<?=$request->id?>" model="callback" data-toggle="modal" href="#modal-request">
								<td class="text-center"><small class="request-id"><?=$request->id?></small></td>
								<td class="nowrap"><?=$request->phone?></td>
								<td>
									<small class="text-muted nowrap">
										<? if (date('d.m.Y') == date('d.m.Y', strtotime($request->date))) : ?>
											Сегодня,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($request->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date('Y') !== date('Y', strtotime($request->date))) : ?>
											<?=Date::format($request->date, 'd.m.Y')?>
										<? else : ?>
											<?=Date::format($request->date, 'd F, H:i')?>
										<? endif; ?>
									</small>
								</td>
								<td><button class="btn btn-danger btn-xs btn has-tooltip request-toarchive" data-placement="left" title="Отправить в архив"><i class="icon-archive"></i></button></td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				<? else : ?>
					<p class="block-empty">Список заявок пуст.</p>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-request" tabindex="-1" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
				<h4 class="modal-title" id="myModalLabel">Заявка <span id="modal-request-id"></span></h4>
			</div>
			<div class="modal-body" id="modal-content">

			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" type="button">Закрыть</button>
			</div>
		</div>
	</div>
</div>