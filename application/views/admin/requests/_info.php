<?=Form::open('/admin/ajax/request', array('id' => 'request-form'))?>
	<?=Form::hidden('request_id','')?>
	<?php if (!empty($request->name)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Имя:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->name?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->phone)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Телефон:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->phone?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->email)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Email:</strong>
			</div>
			<div class="col-md-6">
				<span><a href="mailto:<?=$request->email?>"><?=$request->email?></a></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->zi_email)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Zioniti email:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->zi_email?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->address)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Адрес:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->address?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->subscribe_period)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Период подписки (дней):</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->subscribe_period?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->num_set)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Комплектов:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->num_set?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->num_big_card)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Больших картридеров:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->num_big_card?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->num_flash_card)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Флеш-картридеров:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->num_flash_card?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->num_employees)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Кол-во сотрудников:</strong>
			</div>
			<div class="col-md-6">
				<span><?=$request->num_employees?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->paid_type)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Тип оплаты:</strong>
			</div>
			<div class="col-md-6">
				<span><?=Model_Price::getPaidTypeById($request->paid_type)?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->total_price)) : ?>
		<div class="row">
			<div class="col-md-5">
				<strong>Стоимость ($):</strong>
			</div>
			<div class="col-md-6">
				<span><?=Model_Price::formatNumbers($request->total_price)?></span>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($request->info)) : ?>
		<div class="row request-text">
			<h3>Текст сообщения:</h3>
			<p><?=$request->info?></p>
		</div>
	<?php endif; ?>
<?=Form::close()?>