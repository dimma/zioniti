<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<? if ($requests_callback->count() || $requests_lite->count() || $requests_pro->count() || $requests_server->count()) : ?>
					<table class="table table-hover requests-table">
						<thead>
							<tr>
								<th class="thin-col text-center">ID</th>
								<th>Тип</th>
								<th>Email</th>
								<th>Телефон</th>
								<th class="thin-col text-center">Стоимость ($)</th>
								<th class="thin-col">Дата</th>
							</tr>
						</thead>
						<tbody id="requests-list">
						<? foreach($requests_callback as $request) : ?>
							<tr request_id="<?=$request->id?>" model="callback" data-toggle="modal" href="#modal-request">
								<td class="text-center"><small class="request-id"><?=$request->id?></small></td>
								<td>Обр. звонок</td>
								<td class="nowrap">&nbsp;</td>
								<td class="nowrap"><?=$request->phone?></td>
								<td class="nowrap text-right">&nbsp;</td>
								<td>
									<small class="text-muted nowrap">
										<? if (date('d.m.Y') == date('d.m.Y', strtotime($request->date))) : ?>
											Сегодня,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($request->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date('Y') !== date('Y', strtotime($request->date))) : ?>
											<?=Date::format($request->date, 'd.m.Y')?>
										<? else : ?>
											<?=Date::format($request->date, 'd F, H:i')?>
										<? endif; ?>
									</small>
								</td>
							</tr>
						<? endforeach; ?>
						<? foreach($requests_lite as $request) : ?>
							<tr request_id="<?=$request->id?>" model="lite" data-toggle="modal" href="#modal-request">
								<td class="text-center"><small class="request-id"><?=$request->id?></small></td>
								<td>Lite</td>
								<td class="nowrap"><a href="mailto:<?=$request->email?>"><?=$request->email?></a></td>
								<td class="nowrap">&nbsp;</td>
								<td class="nowrap text-right"><?=Model_Price::formatNumbers($request->total_price)?></td>
								<td>
									<small class="text-muted nowrap">
										<? if (date('d.m.Y') == date('d.m.Y', strtotime($request->date))) : ?>
											Сегодня,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($request->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date('Y') !== date('Y', strtotime($request->date))) : ?>
											<?=Date::format($request->date, 'd.m.Y')?>
										<? else : ?>
											<?=Date::format($request->date, 'd F, H:i')?>
										<? endif; ?>
									</small>
								</td>
							</tr>
						<? endforeach; ?>
						<? foreach($requests_pro as $request) : ?>
							<tr request_id="<?=$request->id?>" model="pro" data-toggle="modal" href="#modal-request">
								<td class="text-center"><small class="request-id"><?=$request->id?></small></td>
								<td>PRO</td>
								<td class="nowrap">&nbsp;</td>
								<td class="nowrap"><?=$request->phone?></td>
								<td class="nowrap text-right"><?=Model_Price::formatNumbers($request->total_price)?></td>
								<td>
									<small class="text-muted nowrap">
										<? if (date('d.m.Y') == date('d.m.Y', strtotime($request->date))) : ?>
											Сегодня,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($request->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date('Y') !== date('Y', strtotime($request->date))) : ?>
											<?=Date::format($request->date, 'd.m.Y')?>
										<? else : ?>
											<?=Date::format($request->date, 'd F, H:i')?>
										<? endif; ?>
									</small>
								</td>
							</tr>
						<? endforeach; ?>
						<? foreach($requests_server as $request) : ?>
							<tr request_id="<?=$request->id?>" model="server" data-toggle="modal" href="#modal-request">
								<td class="text-center"><small class="request-id"><?=$request->id?></small></td>
								<td>Server</td>
								<td class="nowrap"><a href="mailto:<?=$request->email?>"><?=$request->email?></a></td>
								<td class="nowrap"><?=$request->phone?></td>
								<td class="nowrap text-right"><?=Model_Price::formatNumbers($request->total_price)?></td>
								<td>
									<small class="text-muted nowrap">
										<? if (date('d.m.Y') == date('d.m.Y', strtotime($request->date))) : ?>
											Сегодня,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($request->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($request->date, 'H:i')?>
										<? elseif (date('Y') !== date('Y', strtotime($request->date))) : ?>
											<?=Date::format($request->date, 'd.m.Y')?>
										<? else : ?>
											<?=Date::format($request->date, 'd F, H:i')?>
										<? endif; ?>
									</small>
								</td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				<? else : ?>
					<p class="block-empty">Список заявок пуст.</p>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-request" tabindex="-1" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
				<h4 class="modal-title" id="myModalLabel">Заявка <span id="modal-request-id"></span></h4>
			</div>
			<div class="modal-body" id="modal-content">

			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" type="button">Закрыть</button>
			</div>
		</div>
	</div>
</div>