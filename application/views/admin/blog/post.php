<?=Form::open('/admin/ajax/update_post', array('id' => 'add-post-form', 'class' => 'ajax-form'))?>
	<input type="hidden" name="post_id" value="<?=$post->id?>">
	<div class="form-group">
		<input name="title" class="form-control" type="text" placeholder="Заголовок" value="<?=$post->title?>">
	</div>
	<div class="form-group">
		<input name="url" class="form-control input-sm" type="text" placeholder="Псевдоним ссылки" value="<?=$post->url?>">
	</div>
	<div class="form-group upload-block">
		<div class="pull-left">
			<span class="btn fileinput-button">
			  <i class="icon-plus"></i>
			  <span>Загрузить изображение</span>
			  <input data-bfi-disabled multiple name="files" type="file" id="blog-upload">
			</span>
		</div>
		<div class="pull-left" id="post-images-list">
			<? foreach($post->images->find_all() as $image) { ?>

			<? } ?>
		</div>
		<div class="clearfix"></div>
		<div class="progress progress-small" id="blog-img-progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
	</div>
	<div class="form-group">
		<textarea name="text" class="form-control" placeholder="Текст записи"><?=$post->text?></textarea>
	</div>
	<div id="article-submit" class="form-actions form-actions-padding-sm form-actions-padding-md form-actions-padding-lg" style="margin-bottom: 0;">
		<button class="btn btn-success btn-lg"><i class="icon-save"></i>&nbsp;&nbsp;Сохранить</button>
	</div>
<?=Form::close();?>
