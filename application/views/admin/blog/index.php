<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<? if($posts->count()) : ?>
					<table class="table table-hover requests-table">
						<thead>
							<tr>
								<th class="thin-col text-center">&#8470;</th>
								<th>Заголовок</th>
								<th>Статус</th>
								<th class="thin-col">Дата</th>
							</tr>
						</thead>
						<tbody id="posts-list">
						<? foreach($posts as $post) : ?>
							<tr post_id="<?=$post->id?>">
								<td class="text-center"><small class="request-id"><?=$post->id?></small></td>
								<td><?=$post->title?></td>
								<td><?=$post_statuses[$post->status]?></td>
								<td>
									<small class="text-muted nowrap">
										<? if (date('d.m.Y') == date('d.m.Y', strtotime($post->date))) : ?>
											Сегодня,&nbsp;<?=Date::format($post->date, 'H:i')?>
										<? elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($post->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($post->date, 'H:i')?>
										<? elseif (date('Y') !== date('Y', strtotime($post->date))) : ?>
											<?=Date::format($post->date, 'd.m.Y')?>
										<? else : ?>
											<?=Date::format($post->date, 'd F, H:i')?>
										<? endif; ?>
									</small>
								</td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				<? else : ?>
					<p class="block-empty">Нет записей в блоге.</p>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>