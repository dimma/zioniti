<div class="content">
	<div class="bd">
		<h2>Ваш заказ успешно принят</h2>
		<p>Скоро с Вами свяжутся наши специалисты, для подтверждения заказа.</p>
	</div>
	<div class="bd420">
		<p>Предлагаем Вам установить и ознакомится с почтовым приложением Zioniti, с помощью которого, осуществляется вся дальнейшая работа с почтой Zioniti.</p>
		<p><img class="pic-icon" src="/assets/img/download.png" alt=""></p>
		<span class="btn-submit append download crossing"><h5>Скачать Zioniti</h5></span>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>