<div class="table-price">
	<table>
		<tbody>
			<tr>
				<td></td>
				<td><h5>Zioniti <span class="lt">Lite</span></h5></td>
				<td><h5>Zioniti <span class="pr">Pro</span></h5></td>
				<td><h5>Zioniti <span class="sr">Server</span></h5></td>
			</tr>
			<tr>
				<td><h6>Стоимость</h6></td>
				<td><p><?=$price['zioniti_lite'];?>$ / месяц<span>30 дней бесплатно</span></p></td>
				<td><p><?=$price['zioniti_pro'];?>$</p></td>
				<td><p>от <?=$price['zioniti_server'];?>$<sup>1</sup></p></td>
			</tr>
			<tr>
				<td><h6>Количество аккаунтов</h6></td>
				<td><p>1 аккаунт</p></td>
				<td><p>1 аккаунт</p></td>
				<td><p>от 3 аккаунтов</p></td>
			</tr>
			<tr>
				<td><h6>Комплектация</h6></td>
				<td class="no"><span>&nbsp;</span></td>
				<td><p>Смарт-карта<br>Картридер</p></td>
				<td><p>Сервер<br>Смарт-карта<br>Картридер</p></td>
			</tr>
			<tr>
				<td><h6>Шифрование данных<br>AES 256</h6></td>
				<td class="yes"><span>&nbsp;</span></td>
				<td class="yes"><span>&nbsp;</span></td>
				<td class="yes"><span>&nbsp;</span></td>
			</tr>
			<tr>
				<td><h6>Двухфакторная аутентификация</h6></td>
				<td class="no"><span>&nbsp;</span></td>
				<td class="yes"><span>&nbsp;</span></td>
				<td class="yes"><span>&nbsp;</span></td>
			</tr>
			<tr>
				<td><h6>Платформы</h6></td>
				<td><p>Windows, Mac OS</p></td>
				<td><p>Windows, Mac OS</p></td>
				<td><p>Windows, Mac OS</p></td>
			</tr>
			<tr>
				<td><h6>Приоритетная<br>техподдержка</h6></td>
				<td class="no"><span>&nbsp;</span></td>
				<td class="no"><span>&nbsp;</span></td>
				<td class="yes"><span>&nbsp;</span></td>
			</tr>
			<tr>
				<td><h6>Хранение переписки внутри вашей компании</h6></td>
				<td class="no"><span>&nbsp;</span></td>
				<td class="no"><span>&nbsp;</span></td>
				<td class="yes"><span>&nbsp;</span></td>
			</tr>
		</tbody>
	</table>
	<p><sup>1</sup>Стоимость Zioniti SERVER зависит от количества сотрудников ваших партнеров по бизнесу.</p>
</div>