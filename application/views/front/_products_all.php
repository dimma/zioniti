<h2>Выберите свой Zioniti</h2>
<ul class="tariffs">
	<li class="lite">
		<a href="/products/lite">
			<h3>Zioniti <span class="lt">Lite</span></h3>
			<h5><?=$price['zioniti_lite'];?>$ в месяц</h5>
			<img src="/assets/img/tariffs1.png" alt=""/>
			<p>Начальная версия Zioniti.<br> В Zioniti Lite используются стандартные методы шифрования передачи данных.</p>
			<div class="more"><h5>Скачать<span>Первый месяц бесплатно</span></h5></div>
		</a>
	</li>
	<li class="pro">
		<a href="/products/pro">
			<h3>Zioniti <span class="pr">Pro</span></h3>
			<h5><?=$price['zioniti_pro'];?>$</h5>
			<img src="/assets/img/tariffs2.png" alt=""/>
			<p>Оптимальное решение для индивидуальных пользователей.<br> В комплект входит приложение Zioniti, смарт–карта и картридер.</p>
			<div class="more less"><h5>Улучшить до Zioniti PRO</h5></div>
		</a>
	</li>
	<li class="server">
		<a href="/products/server">
			<h3>Zioniti <span class="sr">Server</span></h3>
			<h5>Для фирмы</h5>
			<img src="/assets/img/tariffs3.png" alt=""/>
			<p>Пакет Zioniti для бизнеса. Тариф создан для компаний, которые предпочитают хранить переписку прямо у себя офисе.</p>
			<div class="more less"><h5>Узнать подробнее</h5></div>
		</a>
	</li>
	<div class="clear"></div>
	<?php if (!isset($showOnMainPage)) : ?>
		<br/>
		<p>Все цены указаны в долларах</p>
	<?php endif; ?>
</ul>