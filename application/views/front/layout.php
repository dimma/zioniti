<!DOCTYPE html>
<html>
	<head>
		<title><?=$title?></title>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/x-icon" href="/favicon.ico" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<meta content="telephone=no" name="format-detection">
		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<!--<link rel="apple-touch-icon" href="siteexpo_touch-icon-iphone.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="siteexpo_touch-icon-ipad.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="siteexpo_touch-icon-iphone-retina.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="siteexpo_touch-icon-ipad-retina.png" />-->

		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
		<!--[if lte IE 8]>
		<meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
		<![endif]-->
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body id="main-page">
		<div id="navTop" <?php if ($is_download_page) : ?>class="nav-download"<?php endif; ?>>
			<div class="bd">
				<div id="nav-inner">
					<a id="logo" href="/"><img src="/assets/img/logo.png" alt=""></a>
					<ul id="menu">
						<? foreach($menu as $k => $v) : ?>
							<li><a <? if($controller == $v['url']) { ?>class="active"<? } ?> href="/<?=$v['url']?>"><?=$v['name']?></a></li>
						<? endforeach; ?>
					</ul>
					<div class="select">
						<span id="call" class="call">+7 499 643-83-24</span><img id="arrow-pointer" class="crossing" src="/assets/img/arrow-down.png" alt="">
						<div class="selectbox-call">
							<ul>
								<li>
									<p>Москва</p>
								</li>
								<li>
									<span class="call active">+65 310-80-941</span>
									<p>Лондон</p>
								</li>
								<li>
									<p class="call mailto active"><a href="mailto:hello@zioniti.com?Subject=Hello">hello@zioniti.com</a></p>
								</li>
								<li class="call-back">
									<form action="/ajax/post_callback" name='form_callback' method='POST'>
										<p>Заказать обратный звонок</p>
										<div class="send-request">
											<input type="text" class="phone" name="phone_call" value="" placeholder="Ваш телефон"/>
											<button id="sendCallback">OK</button>
											<div class="clear"></div>
										</div>
										<span class="request">Спасибо за заявку! Скоро с вами свяжется наш специалист.</span>
									</form>
								</li>
							</ul>
						</div>
					</div>
					<form action="/download">
						<button id="download" class="crossing">Скачать</button>
					</form>
				</div>
				<button id="closeBtn">Закрыть видео</button>
			</div>
		</div>
		<div class="wrapper">
			<?=$content?>
		</div>

		<!-- Footer
		================================================== -->
		<div class="footer">
			<div class="bd">
				<ul>
					<li>
						<p>&copy; <? $start = '2014'; if($start == date('Y')) { echo date('Y'); } else { echo $start.'&ndash;'.date('Y'); } ?> Zioniti</p>
						<p>Разработка &mdash; <a href="http://siteexpo.ru/" target="_blank"><span class="logo-footer">&nbsp;</span></a></p>
					</li>
					<li>
						<p>+7 499 643-83-24 &mdash; Москва</p>
						<p>+65 310-80-941 &mdash; Лондон</p>
					</li>
					<li>
						<p><a href="/faq">Вопросы и ответы</a></p>
						<div class="select">
							<span id="locale" class="input">Русский</span>
							<div class="selectbox local">
								<ul>
									<li><a href="#">Английский</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>