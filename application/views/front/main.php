<div class="preview">
	<div class="wrap">
		<h1>Zioniti — безопасная почта</h1>
		<h4>Гарантия конфиденциальности вашей переписки</h4>
		<span id="playBtn" class="crossing"></span>
		<div class="clear"></div>
		<a id="testBtn" class="btn-submit crossing" href="/download"><h4>Попробовать бесплатно<span>Пробная версия 30 дней</span></h4></a>
		<h6>или <a href="/products/order_lite">оформить подписку за <?=$price['zioniti_lite'];?>$ в месяц</a></h6>
	</div>
	<video id="player" controls="controls" tabindex="0" preload="auto">
		<source src="/assets/video/zioniti.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
		<source src="/assets/video/zioniti.webm" type='video/webm; codecs="vp8, vorbis"' />
		<source src="/assets/video/zioniti.ogv" type='video/ogg; codecs="theora, vorbis"' />
	</video>
</div>
<div class="select-your block">
	<div class="bd">
		<?=View::factory('front/_products_all', array('price' => $price, 'showOnMainPage' => TRUE))->render();?>

		<span id="compare"><h4>Сравнить тарифы</h4></span>

		<?=View::factory('front/_products_all_chars', array('price' => $price))->render();?>
	</div>
</div>

<div class="excerpt">
	<h3>Кто владеет информацией, тот всегда на шаг впереди.</h3>
</div>

<div class="landing-info block">
	<div class="bd">
		<ul>
			<li>
				<div class="landing-info-txt">
					<h2>Сверхзащищенная переписка</h2>
					<h4>Ваша переписка — ваше личное дело</h4>
					<p>Zioniti — современный почтовый сервис с высоким уровнем безопасности. Ваши письма видите только вы и адресаты ваших сообщений. Zioniti шифрует письма, прежде чем отправить с компьютера, поэтому даже разработчики Zioniti не могут прочитать вашу переписку.</p>
				</div>
				<span class="landing-info-pic"><img src="/assets/img/info1.png" alt=""/></span>
			</li>
			<div class="clear"></div>
			<li>
				<span class="landing-info-pic"><img src="/assets/img/info2.png" alt=""/></span>
				<div class="landing-info-txt med">
					<h2>Надежная защита данных</h2>
					<h4>Современные способы шифрования RSA и AES</h4>
					<p>При создании Zioniti мы&nbsp;использовали несколько методов шифрования, применяемых в&nbsp;соответствие со&nbsp;стандартом OpenPGP. Информация, связанная с&nbsp;почтой, хранится на&nbsp;смарт-карте, а&nbsp;не&nbsp;на&nbsp;вашем компьютере, поэтому никто не&nbsp;сможет украсть пароли доступа с&nbsp;помощью шпионских программ или взломать переписку.</p>
				</div>
			</li>
			<div class="clear"></div>
			<li>
				<div class="landing-info-txt">
					<h2>Двухфакторная аутентификация</h2>
					<h4>Ограничение доступа третьих лиц к конфиденциальной информации</h4>
					<p>Завладение конфиденциальными данными может нанести серьезный ущерб вашему бизнесу, поэтому для доступа в&nbsp;Zioniti используется двухфакторная аутентификация. Вы&nbsp;прикладываете свою персональную карту к&nbsp;карт-ридеру и&nbsp;вводите пароль. Это обеспечивает дополнительную безопасность переписки.</p>
				</div>
				<span class="landing-info-pic"><img src="/assets/img/info3.png" alt=""/></span>
			</li>
			<div class="clear"></div>
		</ul>
	</div>
</div>

<div class="awards">
	<div class="wrap">
		<h2>Наши награды</h2>
		<h6>Почтовый сервис Zioniti отмечен двумя престижными наградами в области защиты информации на выставках Infosecurity 2013 и ТБ Форум 2014.</h6>
		<img src="/assets/img/awards.png" alt=""/>
	</div>
</div>

<div class="tryout">
	<a id="" class="btn-submit crossing" href="/download"><h4>Попробовать бесплатно</h4></a>
	<h6>или <a href="/products/order_lite">оформить подписку за <?=$price['zioniti_lite'];?>$ в месяц</a></h6>
</div>

<div class="push"></div>