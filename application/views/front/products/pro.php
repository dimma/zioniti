<div class="content">
	<div class="bd">
		<h2>Улучшите до Zioniti <span class="pr">Pro</span></h2>
		<p>Оптимальное решение для индивидуальных пользователей.<br>В комплект входит приложение Zioniti, смарт-карта и картридер.</p>
		<div class="tooltip"></div>
		<p>
			<a href="#" alt="" data-tooltip="Zioniti Lite"><img class="pic-icon" src="/assets/img/tariffs1.png" alt=""></a>
			<img class="pic-icon plus" src="/assets/img/plus.png" alt="">
			<a href="#" alt="" data-tooltip="Большой картридер"><img class="pic-icon" src="/assets/img/big-card.png" alt=""></a>
			<a href="#" alt="" data-tooltip="Карта"><img class="pic-icon" src="/assets/img/map-card.png" alt=""></a>
			<a href="#" alt="" data-tooltip="Флеш-картридер"><img class="pic-icon" src="/assets/img/flash-card.png" alt=""></a>
		</p>
		<a id="" class="btn-submit inner crossing" href="/products/order_pro"><h4>Купить всего за 6 000$</h4></a>
		<div class="specs">
			<h2>Безопасная почта Zioniti</h2>
			<h4>Оптимальное решение для защиты деловой и личной переписки</h4>
			<p>Zioniti PRO&nbsp;&mdash; почтовый сервис, доступ к&nbsp;которому возможен только при использовании персональной смарт-карты и&nbsp;картридера. Ваша переписка хранится на&nbsp;нескольких серверах: часть информации хранится в&nbsp;Европе и&nbsp;Америке и&nbsp;часть на&nbsp;произвольно определенных серверах по&nbsp;всему миру.</p>
		</div>
		<div class="specs">
			<h3>Особенности Zioniti <span class="pr">Pro</span></h3>
			<ul>
				<li><p>Индивидуальная смарт-карта</p></li>
				<li><p>Хранение переписки в зашифровавнном виде</p></li>
				<li><p>Двухфакторная аутентификация</p></li>
				<li><p>Уведомление после каждого входа в почту по смс</p></li>
				<li><p>Определение списка e-mail адресов, с которых вам могут писать</p></li>
				<li><p>Аварийное отключение почты</p></li>
			</ul>
		</div>
		<div class="specs">
			<h3>Комплектация Zioniti <span class="pr">Pro</span></h3>
			<ul>
				<li><p>Приложение Zioniti для Windows и Mac OS</p></li>
				<li><p>Индивидуальная смарт-карта</p></li>
				<li><p>Переносной картридер</p></li>
				<li><p>Инструкция на русском языке</p></li>
				<li><p>Карта восстановления доступа</p></li>
			</ul>
		</div>
		<div class="add">
			<p>Zioniti PRO может использовать только один человек.</p>
			<p>Интересует Zioniti для вашей фирмы? <a href="/products/server">Попробуйте Zioniti SERVER</a></p>
		</div>

	</div>
</div>
<div class="clear"></div>
<div class="push"></div>