<div class="content">
	<div class="bd462">
		<h2>Zioniti <span class="sr">Server</span></h2>
		<h5>Для фирмы</h5>
		<div class="tooltip"></div>
		<p><a href="#" alt="" data-tooltip="Zioniti Server"><img class="pic-icon" src="/assets/img/tariffs3.png" alt=""></a></p>
		<p>Корпоративная версия Zioniti. В комплект входит портативный сервер Zioniti. Аутентификация с помощью смарт–карты и пароля.</p>

		<form action="/ajax/order_server" method="post" accept-charset="utf-8" name="form_order_server">
			<input type="hidden" name="total" value="<?=$price['zioniti_server_int'];?>"/>
			<input type="hidden" name="price_server" value="<?=$price['zioniti_server_int'];?>"/>
			<input type="hidden" name="site_name" value="<?=$siteName;?>" id="siteName"/>

			<div class="form_input">
				<div class="subscription">
					<div class="list-item">
						<label>Количество сотрудников</label>
						<div class="select-num">
							<span class="minus" id="minusNumEmployees"><i>&nbsp;</i></span>
							<input class="num" type="text" name="num_employees" value="<?=$employeesCount?>"/>
							<span class="plus" id="plusNumEmployees"><i>&nbsp;</i></span>
						</div>
						<label id="priceServer"><?=$totalPrice?>$</label>
						<div class="clear"></div>
					</div>
				</div>
				<h5>Контактные данные</h5>
				<input type="text" class="" name="name" placeholder="Имя"/>
				<input type="text" class="" name="phone" placeholder="Телефон"/>
				<input type="text" class="" name="email" placeholder="Электронная почта"/>
			</div>
			<div class="add"><h4>После оформления заказа с вами свяжется наш менеджер, который уточнит и подтвердит заказ.</h4></div>
			<span class="btn-submit inner order crossing" id="submitServer">
				<h4>Купить за <span style="display:inline-block;font-size:22px" id="totalPriceServerButton"><?=$totalPrice?>$</span></h4>
			</span>
		</form>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>