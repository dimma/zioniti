<div class="content">
	<div class="bd">
		<h2>Zioniti <span class="sr">Server</span></h2>
		<p>Расширенный пакет Zioniti SERVER для бизнеса. Zioniti SERVER создан для компаний,<br> которые предпочитают хранить всю переписку прямо у себя в офисе.</p>
		<div class="tooltip"></div>
		<p>
			<a href="#" alt="" data-tooltip="Zioniti Lite"><img class="pic-icon" src="/assets/img/tariffs1.png" alt=""></a>
			<img class="pic-icon plus" src="/assets/img/plus.png" alt="">
			<a href="#" alt="" data-tooltip="Server"><img class="pic-icon" src="/assets/img/server.png" alt=""></a>
			<img class="pic-icon plus" src="/assets/img/plus.png" alt="">
			<a href="#" alt="" data-tooltip="Zioniti Pro"><img class="pic-icon" src="/assets/img/tariffs2.png" alt=""></a>
		</p>
		<a id="" class="btn-submit inner crossing" href="/products/order_server"><h4>Заказать Zioniti SERVER</h4></a>
		<div class="specs">
			<h2>Основа корпоративной безопасности</h2>
			<h4>Система облачно–локального хранения данных Zioniti SERVER</h4>
			<p>Мы устанавливаем в вашем офисе (или любом другом помещении) сервер Zioniti. Вся ваша корпоративная переписка хранится прямо у вас в офисе. В случае необходимости вы можете самостоятельно удалить все свои данные с сервера  Zioniti.</p>
		</div>
		<div class="specs">
			<h3>Особенность Zioniti <span class="sr">Server</span></h3>
			<ul>
				<li><p>Корпоративная переписка хранится внутри компании</p></li>
				<li><p>Надежное шифрование всех данных</p></li>
				<li><p>Двухфакторная аутентификация</p></li>
				<li><p>Гибкая система настроек</p></li>
				<li><p>Возможность передачи данных на резервный сервер</p></li>
				<li><p>Хранение данных в зашифрованном виде</p></li>
			</ul>
		</div>
		<div class="specs">
			<h3>Надежное хранение корпоративной информации</h3>
			<p>Zioniti SERVER обеспечивает надежное хранение всей внутренней и внешней переписки компании. С Zioniti SERVER корпоративная информация закрытого доступа не станет известна третьим лицам.</p>
		</div>
		<div class="specs">
			<h3>Технология  AES (Advanced Encryption Standard) 256</h3>
			<p>В&nbsp;Zioniti SERVER используется современная технология AES 256. Вся переписка шифруется на&nbsp;стороне пользователя и&nbsp;поступает на&nbsp;сервера Zioniti уже в&nbsp;зашифрованном виде. Соответственно, никто кроме пользователя не&nbsp;может прочитать его переписку, в&nbsp;том числе разработчики Zioniti.</p>
		</div>
		<div class="specs">
			<h3>Комплектация Zioniti <span class="sr">Server</span></h3>
			<ul>
				<li><p>Приложение Zioniti для Windows и Mac OS</p></li>
				<li><p>Индивидуальная смарт-карта<sup>1</sup></p></li>
				<li><p>Переносной картридер</p></li>
				<li><p>Инструкция на русском языке</p></li>
				<li><p>Карта восстановления доступа</p></li>
			</ul>
			<br>
			<p><sup>1</sup>Стоимость Zioniti SERVER зависит от количества сотрудников вашей фирмы</p>
		</div>

	</div>
</div>
<div class="clear"></div>
<div class="push"></div>