<div class="content">
	<div class="bd462">
		<h2>Zioniti <span class="lt">Lite</span></h2>
		<h5><?=$price['zioniti_lite']?>$ в месяц</h5>
		<div class="tooltip"></div>
		<p><a href="#" alt="" data-tooltip="Zioniti Lite"><img class="pic-icon" src="/assets/img/tariffs1.png" alt=""></a></p>
		<p>Аутентификация с помощью пароля. Стандартные методы шифрования и передачи данных.</p>

		<form action="/ajax/order_lite" method="post" accept-charset="utf-8" name="form_order_lite">
			<input type="hidden" name="total" value="<?=$price['zioniti_lite_int'];?>"/>
			<input type="hidden" name="price_lite" value="<?=$price['zioniti_lite_int'];?>"/>
			<input type="hidden" name="period" value="1"/>
			<input type="hidden" name="site_name" value="<?=$siteName;?>" id="siteName"/>

			<div class="form_input">
				<div class="subscription">
					<label>Хочу подписку на</label>
					<div class="select">
						<span id="period1" value="1" class="input">1 месяц</span>
						<div class="selectbox">
							<ul>
								<li><a id="itemPeriod3" value="3" href="javascript:void(0);">3 месяца</a></li>
								<li><a id="itemPeriod6" value="6" href="javascript:void(0);">6 месяцев</a></li>
							</ul>
						</div>
					</div>
					<label>за <span id="priceLite"><?=$price['zioniti_lite']?>$</span></label>
					<div class="clear"></div>
				</div>
				<h5>Контактные данные</h5>
				<p>На этот ящик мы отправим вам подтверждение об оплате.</p>
				<input type="text" class="email" placeholder="Электронная почта" name="email"/>
				<h5>Какое имя ящика вы хотите</h5>
				<input type="text" class="email2 zi_email" placeholder="Электронная почта" name="zi_email"/>
				<label>@zioniti.com</label>
				<p class="error-txt" style="display: none">Почтовый ящик с таким именем занят</p>

				<ul class="pay-methods">
					<h3>Способы оплаты</h3>
					<li>
						<input id="L1" type="radio" name="paid_type" hidden="" checked="checked" value="2"/>
						<label for="L1">Электронный перевод</label>
						<p>Платеж с помощью любых банковских карт, сервисов мобильной коммерции (МТС, Билайн, Мегафон), через интернет–банк ведущих банков РФ, банкоматы, терминалы мгновенной оплаты, с помощью приложений для iOS и Android.</p>
					</li>
					<li>
						<input id="L2" type="radio" name="paid_type" hidden="" value="3"/>
						<label for="L2">Безналичный расчет</label>
						<p>Вы&nbsp;загружаете документ с&nbsp;вашими банковскими реквизитами, и&nbsp;вам выставляется счет на&nbsp;оплату. После оплаты счета курьер доставит ваш заказ. При получении заказа выдается товарный чек, содержащий информацию о&nbsp;комплектации заказа и&nbsp;его стоимости.</p>
					</li>
				</ul>
			</div>
			<div class="add"><h4>Персональный ключ активации будет выслан на адрес <span id="emailToSend"></span></h4></div>
			<span class="btn-submit inner order crossing" id="submitLite">
				<h4>
					Купить подписку за
					<span style="display:inline-block;font-size:22px" id="totalPriceLiteButton"><?=$price['zioniti_lite']?>$</span>
					<span>на <span style="display:inline-block;" id="periodButton">1 месяц</span></span>
				</h4>
			</span>
		</form>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>