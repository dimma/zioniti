<div class="content">
	<div class="bd462">
		<h2>Zioniti <span class="pr">Pro</span></h2>
		<h5><?=$price['zioniti_pro'];?>$</h5>
		<div class="tooltip"></div>
		<p><a href="#" alt="" data-tooltip="Zioniti Pro"><img class="pic-icon" src="/assets/img/tariffs2.png" alt=""/></a></p>
		<p>Оптимальное решение для индивидуальных пользователей. В комплект входит приложение Zioniti, смарт–карта и картридер.</p>

		<form action="/ajax/order_pro" method="post" accept-charset="utf-8" name="form_order_pro">
			<input type="hidden" name="total" value="<?=$price['zioniti_pro_int'];?>"/>
			<input type="hidden" name="price_pro" value="<?=$price['zioniti_pro_int'];?>"/>
			<input type="hidden" name="price_big_card" value="<?=$price['big_card_int'];?>"/>
			<input type="hidden" name="price_flash_card" value="<?=$price['flash_card_int'];?>"/>
			<input type="hidden" name="site_name" value="<?=$siteName;?>" id="siteName"/>

			<div class="form_input">
				<div class="subscription">
					<div class="list-item">
						<label>Количество комплектов</label>
						<div class="select-num">
							<span class="minus" id="minusNumPro"><i>&nbsp;</i></span>
							<input class="num" type="text" value="1" name="num_set"/>
							<span class="plus" id="plusNumPro"><i>&nbsp;</i></span>
						</div>
						<label id="pricePro"><?=$price['zioniti_pro'];?>$</label>
						<div class="clear"></div>
					</div>
					<div class="list-item">
						<label class="mtn"><span><img src="/assets/img/b-card.png" alt=""/></span>Большой картридер</label>
						<div class="select-num">
							<span class="minus" id="minusNumBigCard"><i>&nbsp;</i></span>
							<input class="num" type="text" value="0" name="num_big_card"/>
							<span class="plus" id="plusNumBigCard"><i>&nbsp;</i></span>
						</div>
						<label id="priceBigCard">0$</label>
						<div class="clear"></div>
					</div>
					<div class="list-item">
						<label class="mtn"><span><img src="/assets/img/f-card.png" alt=""/></span>Флеш-картридер</label>
						<div class="select-num">
							<span class="minus" id="minusNumFlashCard"><i>&nbsp;</i></span>
							<input class="num" type="text" value="0" name="num_flash_card"/>
							<span class="plus" id="plusNumFlashCard"><i>&nbsp;</i></span>
						</div>
						<label id="priceFlashCard">0$</label>
						<div class="clear"></div>
					</div>
					<div class="list-item">
						<span class="total">Итого: <span id="totalPrice"><?=$price['zioniti_pro'];?>$</span></span>
					</div>

				</div>
				<h5>Контактные данные</h5>
				<input type="text" placeholder="Телефон" name="phone"/>
				<h5>Адрес доставки</h5>
				<input type="text" placeholder="Город, улица, дом, корпус" name="address"/>
				<h5>Дополнительно</h5>
				<textarea placeholder="Детали доставки или ваш комментарий" name="info"></textarea>

				<ul class="pay-methods">
					<h3>Способы оплаты</h3>
					<li>
						<input id="P1" type="radio" name="paid_type" hidden="" value='1' checked="checked"/>
						<label for="P1">Наличными курьеру</label>
						<p>Вы&nbsp;оплачиваете заказ наличными курьеру. При получении заказа мы&nbsp;выдаем товарный чек, содержащий информацию о&nbsp;комплектации заказа и&nbsp;его стоимости.</p>
					</li>
					<li>
						<input id="P2" type="radio" name="paid_type" hidden="" value='2'/>
						<label for="P2">Электронный перевод</label>
						<p>Платеж с помощью любых банковских карт, сервисов мобильной коммерции (МТС, Билайн, Мегафон), через интернет–банк ведущих банков РФ, банкоматы, терминалы мгновенной оплаты, с помощью приложений для iOS и Android.</p>
					</li>
					<li>
						<input id="P3" type="radio" name="paid_type" hidden="" value='3'/>
						<label for="P3">Безналичный расчет</label>
						<p>Вы&nbsp;загружаете документ с&nbsp;вашими банковскими реквизитами, и&nbsp;вам выставляется счет на&nbsp;оплату. После оплаты счета курьер доставит ваш заказ. При получении заказа выдается товарный чек, содержащий информацию о&nbsp;комплектации заказа и&nbsp;его стоимости.</p>
					</li>
				</ul>
			</div>
			<span class="btn-submit inner order crossing" id="submitPro">
				<h4>Купить за <span style="display:inline-block;font-size:22px" id="totalPriceButton"><?=$price['zioniti_pro'];?>$</span></h4>
			</span>
		</form>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>