<div class="content">
	<div class="bd">
		<h2>Zioniti <span class="lt">Lite</span></h2>
		<p>Zioniti Lite — начальная версия почтового сервиса Zioniti, в котором используются стандартные методы шифрования передачи данных. Zioniti Lite распространяется по платной подписке.</p>
		<div class="tooltip"></div>
		<p><a href="#" alt="" data-tooltip="Zioniti Lite"><img class="pic-icon" src="/assets/img/tariffs1.png" alt=""></a></p>

		<a id="" class="btn-submit inner crossing" href="/download"><h4>Попробовать бесплатно<span>Пробная версия 30 дней</span></h4></a>
		<p>или <a href="/products/order_lite">оформить подписку за <?=$price['zioniti_lite']?>$ в месяц</a></p>
		<div class="specs">
			<h3>Особенность Zioniti <span class="lt">Lite</span></h3>
			<ul>
				<li><p>Хранение переписки в зашифрованном виде</p></li>
				<li><p>Уведомление после каждого входа в почту по смс</p></li>
				<li><p>Определение списка e–mail адресов, с которых вам могут писать</p></li>
			</ul>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>