<div class="content">
	<div class="bd">
		<h2>Zioniti — безопасная почта</h2>
	</div>
	<div class="bd420">
		<p>Загрузка вашего Zioniti должна автоматически начаться в течении нескольких секунд. Если этого не произошло, <a href="javascript:history.go(0)">перезагрузите страницу.</a></p>
		<p><img class="pic-icon" src="/assets/img/download.png" alt=""></p>
		<p>После того, как загрузка будет завершена, запустите установку вашего Zioniti.</p>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>