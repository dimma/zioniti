<div class="content inner">
	<div class="bd">
		<h2><?=$post->title?></h2>
		<p class="time"><?=Date::format($post->date, 'd.m.Y')?></p>
		<!--<p class="comment <? if ($post->comments_count) : ?>active<? endif; ?>">
			<? if ($post->comments_count) : ?>
				Комментарии&nbsp;<?=$post->comments_count?>
			<? else : ?>
				Нет комментариев
			<? endif; ?>
		</p>-->
		<div class="pic inner">
			<img src="/assets/upload/blog/<?=$post->images->limit(1)->find()->file?>" alt="">
		</div>
		<p><?=$post->text?></p>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>