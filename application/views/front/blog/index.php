<div class="content inner">
	<div class="bd">
		<? foreach($posts as $post) : ?>
			<section>
				<a href="/blog/<?=$post->url?>">
					<h2><?=$post->title?></h2>
					<p class="time"><?=Date::format($post->date, 'd.m.Y')?></p>
					<!--<p class="comment <? if ($post->comments_count) : ?>active<? endif; ?>">
						<? if ($post->comments_count) : ?>
							Комментарии&nbsp;<?=$post->comments_count?>
						<? else : ?>
							Нет комментариев
						<? endif; ?>
					</p>-->
					<div class="pic">
						<img src="/assets/upload/blog/<?=$post->images->limit(1)->find()->file?>" alt="" />
					</div>
				</a>
				<p><?=Text::limit_chars($post->text, 200, '&hellip;')?></p>
			</section>
		<? endforeach; ?>
	</div>
</div>
<div class="clear"></div>
<div class="push"></div>