<!DOCTYPE html>
	<head>
		<meta charset="utf-8"/>
		<title>Ошибка 404</title>
		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
	</head>
	<body id="error-page">
		<div id="navTop">
			<div class="bd">
				<a id="logo" href="/"><img src="/assets/img/logo.png" alt=""></a>
				<ul id="menu">
					<? foreach($menu as $k => $v) : ?>
						<li><a href="/<?=$v['url']?>"><?=$v['name']?></a></li>
					<? endforeach; ?>
				</ul>
				<div class="select">
					<span id="call" class="call">+7 499 643-83-24</span>
					<div class="selectbox-call">
						<ul>
							<li>
								<p>Москва</p>
							</li>
							<li>
								<span class="call active">+65 310-80-941</span>
								<p>Лондон</p>
							</li>
							<li>
								<p class="call mailto active"><a href="mailto:hello@zioniti.com?Subject=Hello">hello@zioniti.com</a></p>
							</li>
							<li class="call-back">
								<form action="/ajax/post_callback" name='form_callback' method='POST'>
									<p>Заказать обратный звонок</p>
									<div class="send-request">
										<input type="text" class="phone" name="phone_call" value="" placeholder="Ваш телефон"/>
										<button id="sendCallback">OK</button>
										<div class="clear"></div>
									</div>
									<span class="request">Спасибо за заявку! Скоро с вами свяжется наш специалист.</span>
								</form>
							</li>
						</ul>
					</div>
				</div>
				<form action="/download">
					<button id="download" class="crossing">Скачать</button>
				</form>
			</div>
		</div>

		<div class="wrapper">
			<h2 style="text-align:center; padding-top:200px;font-size: 140px;">404</h2>
			<p style="text-align: center; margin-top: -20px;">Такой страницы нет на сайте.</p>
		</div>

		<!-- Footer
		================================================== -->
		<div class="footer">
			<div class="bd">
				<ul>
					<li>
						<p>&copy; 2014 Zioniti</p>
						<p>Разработка &mdash; <a href="http://siteexpo.ru/" target="_blank"><span class="logo-footer">&nbsp;</span></a></p>
					</li>
					<li>
						<p>+7 499 643-83-24 &mdash; Москва</p>
						<p>+65 310-80-941 &mdash; Лондон</p>
					</li>
					<li>
						<p><a href="/faq">Вопросы и ответы</a></p>
						<div class="select">
							<span id="locale" class="input">Русский</span>
							<div class="selectbox local">
								<ul>
									<li><a href="#">Английский</a></li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>