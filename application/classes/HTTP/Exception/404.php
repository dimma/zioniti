<?php defined('SYSPATH') or die('No direct script access.');

class HTTP_Exception_404 extends Kohana_HTTP_Exception_404 {

	/**
	 * Generate a Response for the 404 Exception.
	 *
	 * The user should be shown a nice 404 page.
	 *
	 * @return Response
	 */
	public function get_response()
	{
		$styles['assets/css/style.css'] = 'screen';
		$styles['assets/fonts/stylesheet.css'] = 'screen';

		$scripts[] = 'assets/js/jquery-2.1.1.min.js';
		$scripts[] = 'assets/js/js.js';

		$view = View::factory('errors/404', array('styles' => $styles, 'scripts' => $scripts, 'menu' => Kohana::$config->load('menu')));

		// Remembering that `$this` is an instance of HTTP_Exception_404
		$view->message = $this->getMessage();

		$response = Response::factory()
			->status(404)
			->body($view->render());

		return $response;
	}
}
?>