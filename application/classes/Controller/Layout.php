<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Layout extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'front/layout';

	public function before()
	{
		parent::before();

		# Параметры шаблона
		if($this->auto_render)
		{
			//$detect = new MobileDetect();

			$this->template->controller		= UTF8::strtolower($this->request->controller());
			$this->template->action			= $this->request->action();
			$this->template->menu			= Kohana::$config->load('menu');
			$this->template->title			= ($this->template->controller !== 'main' && isset($this->template->menu[$this->template->controller]['title'])) ? $this->template->menu[$this->template->controller]['title'] : '';
			$this->template->window_title		= $this->template->title;
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= '';
			$this->template->styles			= array();
			$this->template->scripts		= array();
			$this->template->main_page		= ($this->request->controller() == 'Main' && $this->request->action() == 'index');
			$this->template->content		= '';
			$this->template->icon			= '';
			$this->template->rights			= array();
			$this->template->detect			= new MobileDetect();
			$this->template->body_class		= ($this->template->controller !== 'main' && isset($this->template->menu[$this->template->controller]['class'])) ? $this->template->menu[$this->template->controller]['class'] : '';
			$this->template->is_download_page	= ($this->template->action == 'download');
		}
	}

	public function after()
	{
		if($this->auto_render)
		{
			# Подключаем стандартные стили и скрипты
			$styles['assets/css/style.css'] = 'screen';
			$styles['assets/fonts/stylesheet.css'] = 'screen';
			$styles['http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,100italic,300italic,400italic,500italic,700,700italic&subset=latin,cyrillic-ext,cyrillic'] = 'screen';

			/*if($this->template->controller == 'prices' && $this->template->action == 'index')
			{
				$styles['assets/css/style_old.css'] = 'screen';
			}*/

			$scripts[] = 'assets/js/jquery-2.1.1.min.js';

			if($this->template->controller == 'contacts' && $this->template->action == 'index')
			{
				$scripts[] = 'https://maps.googleapis.com/maps/api/js?sensor=false';
			}

			$scripts[] = 'assets/js/js.js';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
		}

		parent::after();
	}
}
?>