<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Requests extends Controller_Admin_Layout {

	public function action_index()
	{
		Controller::redirect('/admin/requests/callback');
	}

	// Callback requests
	public function action_callback()
	{
		$this->template->title = '<small>Заявки</small> / Обратный звонок';
		$data['requests'] = ORM::factory('Requests_Callback')->where('status', '=', '0')->order_by('id', 'DESC')->find_all();
		$this->template->content = View::factory('admin/requests/callback', $data);
	}

	// Zioniti Lite requests
	public function action_lite()
	{
		$this->template->title = '<small>Заявки</small> / Zioniti Lite';
		$data['requests'] = ORM::factory('Requests_Lite')->where('status', '=', '0')->order_by('date', 'DESC')->find_all();
		$this->template->content = View::factory('admin/requests/lite', $data);
	}

	// Zioniti PRO requests
	public function action_pro()
	{
		$this->template->title = '<small>Заявки</small> / Zioniti PRO';
		$data['requests'] = ORM::factory('Requests_Pro')->where('status', '=', '0')->order_by('date', 'DESC')->find_all();
		$this->template->content = View::factory('admin/requests/pro', $data);
	}

	// Zioniti Server requests
	public function action_server()
	{
		$this->template->title = '<small>Заявки</small> / Zioniti Server';
		$data['requests'] = ORM::factory('Requests_Server')->where('status', '=', '0')->order_by('date', 'DESC')->find_all();
		$this->template->content = View::factory('admin/requests/server', $data);
	}

	// Requests in archive
	public function action_archive()
	{
		$this->template->title = '<small>Заявки</small> / Архив';

		$data['requests_callback'] = ORM::factory('Requests_Callback')->where('status', '=', '1')->find_all();
		$data['requests_lite'] = ORM::factory('Requests_Lite')->where('status', '=', '1')->find_all();
		$data['requests_pro'] = ORM::factory('Requests_Pro')->where('status', '=', '1')->find_all();
		$data['requests_server'] = ORM::factory('Requests_Server')->where('status', '=', '1')->find_all();

		$this->template->content = View::factory('admin/requests/archive', $data);
	}

}
