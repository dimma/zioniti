<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Prices extends Controller_Admin_Layout {

	public function action_index()
	{
		$this->template->title = '<small>Цены</small>';

		$data = array(
			'prices' => ORM::factory('Price')->find_all()->as_array(),
		);

		$this->template->content = View::factory('admin/prices/index', $data);
	}
}
