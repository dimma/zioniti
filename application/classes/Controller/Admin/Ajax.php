<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Ajax extends Controller_Admin_Adminajax {

	public function action_index()
	{
		echo '==';
		exit;
	}

	// Add service
	public function action_add_service()
	{
		if($this->request->post())
		{
			$post = $this->request->post();
			$post['sort_order'] = ORM::factory('Service')->where('category','=',$post['category'])->and_where('type','=',$post['type'])->and_where('package','=',$post['package'])->count_all() + 1;
			$post['date'] = Date::formatted_time();

			if(!is_numeric($post['price']))
			{
				unset($post['price']);
			}

			if($post['name'] !== '')
			{
				$service = ORM::factory('Service')->values($post)->save();
				$json_data = array(
					'sort_order' => $service->sort_order,
					'id' => $service->id
				);

				echo json_encode($json_data);
			}
		}

		exit;
	}

	// Delete service
	public function action_delete_service()
	{
		if($this->request->post('id'))
		{
			ORM::factory('service', $this->request->post('id'))->delete();
		}

		exit;
	}

	// Edit price
	public function action_edit_price()
	{
		if( $this->request->post('price_id'))
		{
			$post = $this->request->post();
			$post['price'] = (isset($post['price']) && is_numeric($post['price'])) ? $post['price'] : 0;

			ORM::factory('Price', $post['price_id'])->values($post)->update();
		}

		exit;
	}

	// Sort service
	public function action_sort_service()
	{
		if($this->request->post('id') && $this->request->post('toPosition'))
		{
			$service_id = str_replace('service_','',$this->request->post('id'));

			$service = ORM::factory('service', $service_id);
			$to = $this->request->post('toPosition');
			$from = $this->request->post('fromPosition');

			if($this->request->post('direction') == 'forward')
			{
				DB::update('services')->set(array('sort_order' => DB::expr('sort_order - 1')))->where('sort_order','>=',$from)->and_where('sort_order','<=',$to)->and_where('id','<>',$service_id)->and_where('category','=',$service->category)->and_where('type','=',$service->type)->and_where('package','=',$service->package)->execute();
			}
			elseif($this->request->post('direction') == 'back')
			{
				DB::update('services')->set(array('sort_order' => DB::expr('sort_order + 1')))->where('sort_order','>=',$to)->and_where('sort_order','<=',$from)->and_where('id','<>',$service_id)->and_where('category','=',$service->category)->and_where('type','=',$service->type)->and_where('package','=',$service->package)->execute();
			}

			$service->values(array('sort_order' => $to))->update();
		}

		exit;
	}

	// Archive request
	public function action_archive_request()
	{
		$post = $this->request->post();
		if ($post)
		{
			Kohana::ar($post);
			$model = $this->_getModelName($post['model']);
			if (!empty($model))
			{
				ORM::factory($model, $this->request->post('request_id'))->values(array('status' => '1'))->update();
			}
		}

		exit;
	}

	// Request info
	public function action_request_info()
	{
		$post = $this->request->post();
		if ($post)
		{
			$data = array('request' => array());
			$model = $this->_getModelName($post['model']);
			if (!empty($model))
			{
				$data['request'] = ORM::factory($model, $this->request->post('id'));
			}

			echo View::factory('admin/requests/_info', $data);
		}

		exit;
	}

	// Add post
	public function action_add_post()
	{
		if(isset($_FILES['files']['tmp_name']))
		{
			$type = key($_FILES);
			$name = md5(rand(11111111,99999999).time());
			$ext = strtolower(pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION));
			$file_name = $name.'.'.$ext;

			Upload::save($_FILES[$type], $file_name, DOCROOT.'assets/upload/blog/_temp');

			/*
			$file_data = array(
				'name' => $name,
				'file' => $file_name,
				'ext' => $ext,
				'date' => Date::formatted_time(),
			);

			$image = ORM::factory('Postimage')->values($file_data)->save();
			*/

			$json = array(
				'src' => '/assets/upload/blog/_temp/'.$file_name,
				'file' => $file_name
			);

			echo json_encode($json);
		}
		elseif($this->request->post() && !$_FILES)
		{
			try
			{
				$_post = $this->request->post();
				$_post['date'] = Date::formatted_time();
				$_post['status'] = 1;

				$post = ORM::factory('Post')->values($_post)->save();

				if($this->request->post('images'))
				{
					foreach($this->request->post('images') as $image)
					{
						copy(DOCROOT.'assets/upload/blog/_temp/'.$image, DOCROOT.'assets/upload/blog/'.$image);
						unlink(DOCROOT.'assets/upload/blog/_temp/'.$image);

						$a_name = explode('.',$image);
						ORM::factory('Postimage')->values(array(
							'name' => $a_name[0],
							'ext' => strtolower(pathinfo($image, PATHINFO_EXTENSION)),
							'file' => $image,
							'date' => Date::formatted_time(),
							'post_id' => $post->id
						))->save();
					}
				}

				$json['status'] = 1;
			}
			catch(ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}

		exit;
	}

	// Update post
	public function action_update_post()
	{
		if(isset($_FILES['files']['tmp_name']))
		{
			$type = key($_FILES);
			$name = md5(rand(11111111,99999999).time());
			$ext = strtolower(pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION));
			$file_name = $name.'.'.$ext;

			Upload::save($_FILES[$type], $file_name, DOCROOT.'assets/upload/blog/_temp');

			/*
			$file_data = array(
				'name' => $name,
				'file' => $file_name,
				'ext' => $ext,
				'date' => Date::formatted_time(),
			);

			$image = ORM::factory('Postimage')->values($file_data)->save();
			*/

			$json = array(
				'src' => '/assets/upload/blog/_temp/'.$file_name,
				'file' => $file_name
			);

			echo json_encode($json);
		}
		elseif($this->request->post() && !$_FILES)
		{
			try
			{
				$_post = $this->request->post();
				$post = ORM::factory('Post', $this->request->post('post_id'));

				$post->values($_post)->update();

				if($this->request->post('images'))
				{
					foreach($this->request->post('images') as $image)
					{
						copy(DOCROOT.'assets/upload/blog/_temp/'.$image, DOCROOT.'assets/upload/blog/'.$image);
						unlink(DOCROOT.'assets/upload/blog/_temp/'.$image);

						$a_name = explode('.',$image);
						ORM::factory('Postimage')->values(array(
							'name' => $a_name[0],
							'ext' => strtolower(pathinfo($image, PATHINFO_EXTENSION)),
							'file' => $image,
							'date' => Date::formatted_time(),
							'post_id' => $post->id
						))->save();
					}
				}

				$json['status'] = 1;
			}
			catch(ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}


		exit;
	}

	/**
	 * Return model name like 'Requests_*'
	 * @access private
	 * @param string $postModel
	 * @return string
	 */
	private function _getModelName($postModel)
	{
		$model = '';
		if (!empty($postModel))
		{
			switch ($postModel)
			{
				case 'callback':
					$model = 'Requests_Callback';
				break;

				case 'lite':
					$model = 'Requests_Lite';
				break;

				case 'pro':
					$model = 'Requests_Pro';
				break;

				case 'server':
					$model = 'Requests_Server';
				break;
			}
		}

		return $model;
	}
}
