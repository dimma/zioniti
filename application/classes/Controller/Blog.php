<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Blog extends Controller_Layout {

	// Blog
	public function action_index()
	{
		$data['posts'] = ORM::factory('Post')->find_all();
		$this->template->content = View::factory('front/blog/index', $data);
	}

	// Post
	public function action_post()
	{
		$id = $this->request->param('id');
		$data['post'] = ORM::factory('Post', array('url' => $id));

		if($data['post']->id)
		{
			$this->template->content = View::factory('front/blog/article', $data);
		}
		else
		{
			throw HTTP_Exception::factory(404);
		}
	}
}