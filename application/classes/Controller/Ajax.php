<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller
{
	public function action_order_pro()
	{
		$post = $this->request->post();

		if (!empty($post))
		{
			if ($post['num_set'] !== '' && !empty($post['phone']) && Valid::phone($post['phone'], 11))
			{
				$post['phone'] = preg_replace('/\D+/', '', $post['phone']);
				$post['phone'] = sprintf("%s %s %s-%s-%s",
					str_replace('8', '+7', substr($post['phone'], 0, 1)),
					substr($post['phone'], 1, 3),
					substr($post['phone'], 4, 3),
					substr($post['phone'], 7, 2),
					substr($post['phone'], 9, 2)
				);
				$post['num_big_card'] = empty($post['num_big_card']) ? 0 : $post['num_big_card'];
				$post['num_flash_card'] = empty($post['num_flash_card']) ? 0 : $post['num_flash_card'];

				$prices = Model_Price::getPrices();
				$totalPrice = $prices['zioniti_pro_int'] * $post['num_set']
					+ $prices['big_card_int'] * $post['num_big_card']
					+ $prices['flash_card_int'] * $post['num_flash_card'];

				$data = array(
					'num_set' => $post['num_set'],
					'num_big_card' => $post['num_big_card'],
					'num_flash_card' => $post['num_flash_card'],
					'total_price' => $totalPrice,
					'phone' => $post['phone'],
					'address' => empty($post['address']) ? '' : $post['address'],
					'info' => empty($post['info']) ? '' : $post['info'],
					'paid_type' => $post['paid_type'],
					'date' => Date::formatted_time(),
				);

				ORM::factory('Requests_Pro')->values($data)->save();

				$text  = '<p>Поступила заявка на Zioniti PRO с сайта <a href="http://' . Controller_Main::SITE_NAME . '">' . Controller_Main::SITE_NAME . '</a></p>';
				$text .= '<p>Кол-во комплектов: '.$post['num_set'].'</p>';

				if (!empty($post['num_big_card']))
				{
					$text .= '<p>Больших картридеров: '.$post['num_big_card'].'</p>';
				}
				if (!empty($post['num_flash_card']))
				{
					$text .= '<p>Флеш-картридеров: '.$post['num_flash_card'].'</p>';
				}

				$text .= '<p>Тип оплаты: '.$post['paid_type'].'<br/>Итого: '.Model_Price::formatNumbers($totalPrice).'$</p>';
				$text .= '<p>Тел.: '.$post['phone'].'<br />Адрес: '.$post['address'].'</p>';

				if (!empty($post['info']))
				{
					$text .= '<p>Дополнительно:<br />'.$post['info'].'</p>';
				}

				Email::factory('Заявка с сайта ' . Controller_Main::SITE_NAME . ' - Zioniti PRO')
					->to('dimma@xsoft.org')
					->to('confirmation@zioniti.com')
					->from('robot@zioniti.com', Controller_Main::SITE_NAME)
					->message($text, 'text/html')
					->send();

				$json_data['sent'] = 1;
			}
			else
			{
				foreach($post as $k => $v)
				{
					if ($v == '' && $k == 'phone')
					{
						$json_data['errors'][$k] = 'not_empty';
					}
				}

				if (!Valid::phone($post['phone']))
				{
					$json_data['errors']['phone'] = 'not_valid';
				}
			}

			echo json_encode($json_data);
		}

		exit;
	}

	public function action_order_lite()
	{
		$post = $this->request->post();

		if (!empty($post))
		{
			$json_data = array();
			$ziEmail = $post['zi_email'] . '@zioniti.com';

			if (!empty($post['period']) && !empty($post['email']) && !empty($post['zi_email']) && Valid::email($post['email']))
			{
				$prices = Model_Price::getPrices();
				$totalPrice = $prices['zioniti_lite_int'] * $post['period'];
				$defaultDaysInPeriod = 30;
				$period = $defaultDaysInPeriod;

				switch ($post['period'])
				{
					case 1:
					case 3:
					case 6:
						$period = $post['period'] * $defaultDaysInPeriod;
					break;
				}

				$data = array(
					'subscribe_period' => $period,
					'total_price' => $totalPrice,
					'email' => $post['email'],
					'zi_email' => $ziEmail,
					'paid_type' => $post['paid_type'],
					'date' => Date::formatted_time(),
				);

				ORM::factory('Requests_Lite')->values($data)->save();

				$text  = '<p>Поступила заявка на Zioniti Lite с сайта <a href="http://' . Controller_Main::SITE_NAME . '">' . Controller_Main::SITE_NAME . '</a></p>';
				$text .= '<p>Кол-во месяцев подписки: '.$post['period'].'<br/>Тип оплаты: '.$post['paid_type'].'</p>';
				$text .= '<p>Email: '.$post['email'].'<br/>Zioniti email: '.$ziEmail.'<br/>Итого: '.Model_Price::formatNumbers($totalPrice).'$</p>';

				Email::factory('Заявка с сайта ' . Controller_Main::SITE_NAME . ' - Zioniti Lite')
					->to('dimma@xsoft.org')
					->to('confirmation@zioniti.com')
					->from('robot@zioniti.com', Controller_Main::SITE_NAME)
					->message($text, 'text/html')
					->send();

				$json_data['sent'] = 1;
			}
			else
			{
				foreach($post as $k => $v)
				{
					if ($v == '' && ($k == 'email' || $k == 'zi_email'))
					{
						$json_data['errors'][$k] = 'not_empty';
					}
				}

				if (!empty($post['email']) && !filter_var($post['email'], FILTER_VALIDATE_EMAIL))
				{
					$json_data['errors']['email'] = 'not_valid';
				}
				if (!empty($post['zi_email']) && !filter_var($ziEmail, FILTER_VALIDATE_EMAIL))
				{
					$json_data['errors']['zi_email'] = 'not_valid';
				}
			}

			echo json_encode($json_data);
		}

		exit;
	}

	public function action_order_server()
	{
		$post = $this->request->post();

		if (!empty($post))
		{
			if ($post['num_employees'] !== '' && !empty($post['phone']) && Valid::phone($post['phone'], 11))
			{
				$post['phone'] = preg_replace('/\D+/', '', $post['phone']);
				$post['phone'] = sprintf("%s %s %s-%s-%s",
					str_replace('8', '+7', substr($post['phone'], 0, 1)),
					substr($post['phone'], 1, 3),
					substr($post['phone'], 4, 3),
					substr($post['phone'], 7, 2),
					substr($post['phone'], 9, 2)
				);

				$prices = Model_Price::getPrices();
				$totalPrice = $prices['zioniti_server_int'] * $post['num_employees'];

				$data = array(
					'num_employees' => $post['num_employees'],
					'name' => $post['name'],
					'phone' => $post['phone'],
					'email' => empty($post['email']) ? '' : $post['email'],
					'total_price' => $totalPrice,
					'date' => Date::formatted_time(),
				);

				ORM::factory('Requests_Server')->values($data)->save();

				$text  = '<p>Поступила заявка на Zioniti Server с сайта <a href="http://' . Controller_Main::SITE_NAME . '">' . Controller_Main::SITE_NAME . '</a></p>';
				$text .= '<p>Кол-во сотрудников: '.$post['num_employees'].'</p>';

				if (!empty($post['name']))
				{
					$text .= '<p>Имя: '.$post['name'].'</p>';
				}
				if (!empty($post['email']))
				{
					$text .= '<p>Email: '.$post['email'].'</p>';
				}

				$text .= '<p>Тел.: '.$post['phone'].'<br />Итого: '.Model_Price::formatNumbers($totalPrice).'$</p>';

				Email::factory('Заявка с сайта ' . Controller_Main::SITE_NAME . ' - Zioniti Server')
					->to('dimma@xsoft.org')
					->to('confirmation@zioniti.com')
					->from('robot@zioniti.com', Controller_Main::SITE_NAME)
					->message($text, 'text/html')
					->send();

				$json_data['sent'] = 1;
			}
			else
			{
				foreach($post as $k => $v)
				{
					if ($v == '' && $k == 'phone')
					{
						$json_data['errors'][$k] = 'not_empty';
					}
				}

				if (!Valid::phone($post['phone']))
				{
					$json_data['errors']['phone'] = 'not_valid';
				}
				if (!empty($post['email']) && !filter_var($post['email'], FILTER_VALIDATE_EMAIL))
				{
					$json_data['errors']['email'] = 'not_valid';
				}
			}

			echo json_encode($json_data);
		}

		exit;
	}

	public function action_post_callback()
	{
		$post = $this->request->post();
		if (!empty($post['phone']) && Valid::phone($post['phone'], 11))
		{
			$data = array();
			$post['phone'] = preg_replace('/\D+/', '', $post['phone']);
			$data['phone'] = sprintf("%s %s %s-%s-%s",
				str_replace('8', '+7', substr($post['phone'], 0, 1)),
				substr($post['phone'], 1, 3),
				substr($post['phone'], 4, 3),
				substr($post['phone'], 7, 2),
				substr($post['phone'], 9, 2)
			);
			$data['date'] = Date::formatted_time();

			ORM::factory('Requests_Callback')->values($data)->save();

			echo 'OK';
		}

		exit;
	}

}
?>