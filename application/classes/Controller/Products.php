<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Products extends Controller_Layout {

	// Products
	public function action_index()
	{
		$data = array('price' => Model_Price::getPrices());
		$this->template->title = 'Zioniti — Продукты';
		$this->template->content = View::factory('front/products/index', $data);
	}

	// Zioniti PRO
	public function action_pro()
	{
		$data = array('price' => Model_Price::getPrices());
		$this->template->title = 'Zioniti — Zioniti PRO';
		$this->template->content = View::factory('front/products/pro', $data);
	}

	// Zioniti Lite
	public function action_lite()
	{
		$data = array('price' => Model_Price::getPrices());
		$this->template->title = 'Zioniti — Zioniti Lite';
		$this->template->content = View::factory('front/products/lite', $data);
	}

	// Zioniti Server
	public function action_server()
	{
		$data = array('price' => Model_Price::getPrices());
		$this->template->title = 'Zioniti — Zioniti Server';
		$this->template->content = View::factory('front/products/server', $data);
	}

	// Order Zioniti PRO
	public function action_order_pro()
	{
		$data = array(
			'price' => Model_Price::getPrices(),
			'siteName' => Controller_Main::SITE_NAME,
		);

		$this->template->title = 'Zioniti — Улучшить до Zioniti PRO';
		$this->template->content = View::factory('front/products/order_pro', $data);
	}

	// Order Zioniti Lite
	public function action_order_lite()
	{
		$data = array(
			'price' => Model_Price::getPrices(),
			'siteName' => Controller_Main::SITE_NAME,
		);

		$this->template->title = 'Zioniti — Улучшить до Zioniti Lite';
		$this->template->content = View::factory('front/products/order_lite', $data);
	}

	// Order Zioniti Server
	public function action_order_server()
	{
		$price = Model_Price::getPrices();
		$employeesCount = 20;
		$data = array(
			'price'			=> $price,
			'employeesCount'	=> $employeesCount,
			'totalPrice'		=> Model_Price::formatNumbers($employeesCount * $price['zioniti_server']),
			'siteName'		=> Controller_Main::SITE_NAME,
		);
		$this->template->title = 'Zioniti — Улучшить до Zioniti Server';
		$this->template->content = View::factory('front/products/order_server', $data);
	}
	
}