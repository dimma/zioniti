<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Business extends Controller_Layout {

	// Business solutions
	public function action_index()
	{
		$data = array('price' => Model_Price::getPrices());

		$this->template->body_class = 'main';
		$this->template->title = 'Бизнес-решения';
		$this->template->content = View::factory('front/business/index', $data);
	}

}