<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Layout {

	const SITE_NAME = 'zioniti.local';

	// Main page
	public function action_index()
	{
		$data = array('price' => Model_Price::getPrices());

		$this->template->title = 'Zioniti';
		$this->template->content = View::factory('front/main', $data);
	}
	
	// Download page
	public function action_download()
	{
		$data = array();

		$this->template->title = 'Zioniti — Загрузка';
		$this->template->content = View::factory('front/download', $data);
	}
	
	// Download accept page
	public function action_download_accept()
	{
		$data = array();

		$this->template->title = 'Zioniti — Ваш заказ успешно принят';
		$this->template->content = View::factory('front/download_accept', $data);
	}
	
	// FAQ page
	public function action_faq()
	{
		$data = array();

		$this->template->title = 'Zioniti — Вопросы и ответы';
		$this->template->content = View::factory('front/faq', $data);
	}

}