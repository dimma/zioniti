<?php defined('SYSPATH') or die('No direct script access.');

class Model_Price extends ORM
{
	// Format numbers (1 000, 23 000 329, 32 023, ...)
	static public function formatNumbers($num)
	{
		return number_format($num, 0, ',', ' ');
	}

	/**
	 * Get paid type by id 'paid_type' from tables 'zi_requests_lites', 'zi_requests_pros'
	 * @access static public
	 * @param int id
	 * @return string
	 */
	static public function getPaidTypeById($id)
	{
		$types = array(
			1 => 'Наличными курьеру',
			2 => 'Электронный перевод',
			3 => 'Безналичный расчет',
		);
		return empty($types[$id]) ? '' : $types[$id];
	}
	
	// Get all prices
	static public function getPrices()
	{
		$res = array();
		$prices = ORM::factory('Price')->find_all()->as_array();
		foreach ($prices as $p)
		{
			// formatted price
			$res[$p->name] = Model_Price::formatNumbers($p->price);
			// integer price
			$res[$p->name . '_int'] = $p->price;
		}
		
		if (empty($res))
		{
			$res = array(
				'zioniti_lite' => '30',
				'zioniti_pro' => '6 000',
				'zioniti_server' => '600',
				'big_card' => '0',
				'flash_card' => '0',
				'zioniti_lite_int' => 30,
				'zioniti_pro_int' => 6000,
				'zioniti_server_int' => 600,
				'big_card_int' => 0,
				'flash_card_int' => 0,
			);
		}
		
		return $res;
	}
}
?>