<?php defined('SYSPATH') or die('No direct script access.');

class Model_Postimage extends ORM
{
	protected $_belongs_to = array(
		'post' => array('model' => 'Post', 'foreign_key' => 'post_id')
	);
}
?>