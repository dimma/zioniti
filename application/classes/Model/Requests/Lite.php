<?php defined('SYSPATH') or die('No direct script access.');

class Model_Requests_Lite extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				array('Security::xss_clean'),
			),
		);
	}

	public function rules()
	{
		return array(
			'email'	=> array(array('Valid::email')),
		);
	}

	protected $_sorting = array('date' => 'DESC');
}
?>