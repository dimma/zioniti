<?php defined('SYSPATH') or die('No direct script access.');

class Model_Requests_Pro extends ORM
{
	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
				array('HTML::entities'),
				array('Security::xss_clean'),
			),
		);
	}

	public function rules()
	{
		return array(
			'phone' => array(array('not_empty'), array('Valid::phone'))
		);
	}

	protected $_sorting = array('date' => 'DESC');
}
?>