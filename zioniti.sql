-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 04 2014 г., 12:59
-- Версия сервера: 5.5.39
-- Версия PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `zioniti`
--

-- --------------------------------------------------------

--
-- Структура таблицы `zi_postimages`
--

CREATE TABLE IF NOT EXISTS `zi_postimages` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ext` varchar(32) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `zi_postimages`
--

INSERT INTO `zi_postimages` (`id`, `name`, `ext`, `file`, `date`, `post_id`) VALUES
(1, '171832b36634d42e072194be38df47a9', 'jpg', '171832b36634d42e072194be38df47a9.jpg', '2014-08-12 16:34:51', 1),
(2, '0bbb0c7eba03f1e5771265d09b956f23', 'jpg', '0bbb0c7eba03f1e5771265d09b956f23.jpg', '2014-09-03 10:37:23', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `zi_posts`
--

CREATE TABLE IF NOT EXISTS `zi_posts` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `url` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `comments_count` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `zi_posts`
--

INSERT INTO `zi_posts` (`id`, `title`, `text`, `url`, `date`, `status`, `comments_count`) VALUES
(1, 'Почта для работы', 'Деловая переписка — основа бизнес коммуникаций. Представители компаний пересылают друг другу сведения о сделках, клиентах и контрагентах, в переписке между сотрудниками фирмы встречается информация, составляющая коммерческую тайну. Для защиты коммерческой тайны необходим надежный почтовый сервис.\r\n\r\nДеловая переписка — основа бизнес коммуникаций. Представители компаний пересылают друг другу сведения о сделках, клиентах и контрагентах, в переписке между сотрудниками фирмы встречается информация, составляющая коммерческую тайну. Для защиты коммерческой тайны необходим надежный почтовый сервис.\r\n\r\nДеловая переписка — основа бизнес коммуникаций. Представители компаний пересылают друг другу сведения о сделках, клиентах и контрагентах, в переписке между сотрудниками фирмы встречается информация, составляющая коммерческую тайну. Для защиты коммерческой тайны необходим надежный почтовый сервис', 'razrabotka_kachestvennogo_sajta', '2014-08-29 10:19:36', 1, 10),
(2, 'Test', 'Для лучшей производительности, все значки нуждаются базовом классе и индивидуальном классе значков. Разместите следующий код в любом месте. Не забудьте поставить пробел между значком и текстом для правильного отступ (padding).\nНе смешивайте с другими компонентами\n\nКлассы значков не могут сочетаться с другими элементами. Они спроектированы, чтобы быть отдельными элементами. Вместо этого добавте <span> и применить значок классов <span>\n', 'test_url', '2014-09-03 10:30:22', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `zi_prices`
--

CREATE TABLE IF NOT EXISTS `zi_prices` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `zi_prices`
--

INSERT INTO `zi_prices` (`id`, `name`, `price`, `title`) VALUES
(1, 'zioniti_lite', 30, 'Zioniti Lite'),
(2, 'zioniti_pro', 6000, 'Zioniti PRO'),
(3, 'zioniti_server', 600, 'Zioniti Server'),
(4, 'big_card', 1000, 'Большой картридер'),
(5, 'flash_card', 500, 'Флеш-картридер');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_requests_callbacks`
--

CREATE TABLE IF NOT EXISTS `zi_requests_callbacks` (
`id` int(11) unsigned NOT NULL,
  `phone` varchar(32) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `zi_requests_callbacks`
--

INSERT INTO `zi_requests_callbacks` (`id`, `phone`, `status`, `date`) VALUES
(1, '7 499 643-83-24', 0, '2014-09-02 11:44:03'),
(2, '7 499 643-83-24', 0, '2014-09-02 12:10:20'),
(3, '7 988 222-33-33', 0, '2014-09-03 15:05:15'),
(4, '7 499 643-83-24', 0, '2014-09-03 17:43:30'),
(5, '7 499 643-83-2', 0, '2014-09-03 17:47:28'),
(6, '7 999 222-22-22', 1, '2014-09-03 17:51:45');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_requests_lites`
--

CREATE TABLE IF NOT EXISTS `zi_requests_lites` (
`id` int(11) unsigned NOT NULL,
  `subscribe_period` int(11) DEFAULT '30',
  `email` varchar(128) NOT NULL,
  `zi_email` varchar(128) NOT NULL,
  `total_price` int(11) NOT NULL,
  `paid_type` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `zi_requests_lites`
--

INSERT INTO `zi_requests_lites` (`id`, `subscribe_period`, `email`, `zi_email`, `total_price`, `paid_type`, `status`, `date`) VALUES
(1, 30, 'dmmsskn@gmail.com', 'dmmsskn@zioniti.com', 30, 2, 0, '2014-09-02 16:27:15'),
(2, 30, 'dmmsskn@gmail.com', 'dmmsskn1@zioniti.com', 30, 2, 0, '2014-09-02 16:35:00'),
(3, 180, 'dmmsskn@gmail.com', 'dmmsskn2@zioniti.com', 180, 2, 0, '2014-09-02 16:36:17'),
(4, 180, 'dmmsskn@gmail.com', 'df@zioniti.com', 180, 2, 0, '2014-09-02 16:47:06'),
(5, 90, 'dmmsskn@gmail.com', 'daaaaa@zioniti.com', 90, 2, 1, '2014-09-03 14:58:46');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_requests_pros`
--

CREATE TABLE IF NOT EXISTS `zi_requests_pros` (
`id` int(11) unsigned NOT NULL,
  `num_set` int(11) unsigned NOT NULL DEFAULT '1',
  `num_big_card` int(11) unsigned DEFAULT '0',
  `num_flash_card` int(11) unsigned DEFAULT '0',
  `total_price` int(11) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `info` text,
  `paid_type` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `zi_requests_pros`
--

INSERT INTO `zi_requests_pros` (`id`, `num_set`, `num_big_card`, `num_flash_card`, `total_price`, `phone`, `address`, `info`, `paid_type`, `status`, `date`) VALUES
(1, 1, 0, 0, 6000, '7 888 999-88-88', '', NULL, 1, 0, '2014-09-02 12:40:49'),
(2, 1, 0, 0, 6000, '7 888 999-88-88', '', NULL, 1, 0, '2014-09-02 12:44:49'),
(3, 1, 0, 0, 6000, '7 888 999-88-88', '', NULL, 1, 0, '2014-09-02 12:45:28'),
(4, 1, 0, 0, 6000, '7 888 999-88-88', '', NULL, 1, 0, '2014-09-02 12:45:48'),
(5, 1, 0, 0, 6000, '7 888 999-88-88', '', NULL, 1, 0, '2014-09-02 12:46:01'),
(6, 2, 1, 1, 13500, '7 888 999-88-88', '', NULL, 2, 0, '2014-09-02 12:46:53'),
(7, 2, 1, 1, 13500, '7 888 999-88-88', '', NULL, 2, 0, '2014-09-02 12:48:29'),
(8, 2, 1, 1, 13500, '7 888 999-88-88', '', NULL, 2, 0, '2014-09-02 14:06:03'),
(9, 2, 1, 1, 13500, '7 888 999-88-88', 'asdf', NULL, 2, 0, '2014-09-02 14:06:15'),
(10, 2, 1, 1, 13500, '7 888 999-88-88', 'sdfdersdf', 'asdf', 2, 0, '2014-09-02 14:06:50'),
(11, 2, 1, 2, 14000, '7 888 999-88-88', 'sdfdersdf', 'asdf', 2, 0, '2014-09-02 14:12:43'),
(12, 2, 1, 2, 14000, '7 888 999-88-88', 'sdfdersdf', 'asdf', 2, 0, '2014-09-02 14:14:35'),
(13, 2, 1, 2, 14000, '7 888 999-88-88', 'sdfdersdf', 'asdf', 2, 0, '2014-09-02 14:15:25'),
(14, 1, 0, 0, 6000, '7 898 999-88-88', '', '', 1, 0, '2014-09-02 14:33:08'),
(15, 6, 2, 1, 38500, '7 666 999-22-22', 'aaaaaaaaaa', '2234234', 2, 0, '2014-09-03 15:02:02'),
(16, 1, 0, 0, 6000, '7 999 888-22-22', '', '', 1, 1, '2014-09-03 17:40:17');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_requests_servers`
--

CREATE TABLE IF NOT EXISTS `zi_requests_servers` (
`id` int(11) unsigned NOT NULL,
  `num_employees` int(11) unsigned NOT NULL DEFAULT '1',
  `name` varchar(128) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `total_price` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `zi_requests_servers`
--

INSERT INTO `zi_requests_servers` (`id`, `num_employees`, `name`, `phone`, `email`, `total_price`, `status`, `date`) VALUES
(1, 999, 'nametest', '7 999 888-77-77', 'dmmsskn@gmail.com', 599400, 0, '2014-09-02 17:35:57'),
(2, 34, 'nametest', '7 999 888-77-77', 'dmmsskn@gmail.com', 20400, 0, '2014-09-02 17:36:32'),
(3, 19, 'Dadfas', '7 999 000-22-22', 'dmmsskn@gmail.com', 11400, 1, '2014-09-03 15:06:14');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_roles`
--

CREATE TABLE IF NOT EXISTS `zi_roles` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `zi_roles`
--

INSERT INTO `zi_roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.'),
(3, 'requests', ''),
(4, 'blog', ''),
(5, 'prices', '');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_roles_users`
--

CREATE TABLE IF NOT EXISTS `zi_roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zi_roles_users`
--

INSERT INTO `zi_roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `zi_users`
--

CREATE TABLE IF NOT EXISTS `zi_users` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `zi_users`
--

INSERT INTO `zi_users` (`id`, `name`, `email`, `username`, `password`, `logins`, `last_login`, `date`) VALUES
(1, 'Администратор', 'rustam@otono.ru', 'admin', '2eec780143f4eedd4054bf9a811692f1a5563166cbd090eb1cbe1b7c4a893e8b', 43, 1409811025, '2014-03-18 16:08:23');

-- --------------------------------------------------------

--
-- Структура таблицы `zi_user_tokens`
--

CREATE TABLE IF NOT EXISTS `zi_user_tokens` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `zi_user_tokens`
--

INSERT INTO `zi_user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`) VALUES
(1, 1, '7e59df8b3d2e55069406031c25610a99ac9401fa', '3b7f62c12943686014908b0b01e419a747c7150e', 1409811025, 1411020625);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zi_postimages`
--
ALTER TABLE `zi_postimages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zi_posts`
--
ALTER TABLE `zi_posts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zi_prices`
--
ALTER TABLE `zi_prices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zi_requests_callbacks`
--
ALTER TABLE `zi_requests_callbacks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zi_requests_lites`
--
ALTER TABLE `zi_requests_lites`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `zi_email` (`zi_email`);

--
-- Indexes for table `zi_requests_pros`
--
ALTER TABLE `zi_requests_pros`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zi_requests_servers`
--
ALTER TABLE `zi_requests_servers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zi_roles`
--
ALTER TABLE `zi_roles`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_name` (`name`);

--
-- Indexes for table `zi_roles_users`
--
ALTER TABLE `zi_roles_users`
 ADD PRIMARY KEY (`user_id`,`role_id`), ADD KEY `fk_role_id` (`role_id`);

--
-- Indexes for table `zi_users`
--
ALTER TABLE `zi_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_username` (`username`), ADD UNIQUE KEY `uniq_email` (`email`);

--
-- Indexes for table `zi_user_tokens`
--
ALTER TABLE `zi_user_tokens`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_token` (`token`), ADD KEY `fk_user_id` (`user_id`), ADD KEY `expires` (`expires`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zi_postimages`
--
ALTER TABLE `zi_postimages`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zi_posts`
--
ALTER TABLE `zi_posts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zi_prices`
--
ALTER TABLE `zi_prices`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zi_requests_callbacks`
--
ALTER TABLE `zi_requests_callbacks`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `zi_requests_lites`
--
ALTER TABLE `zi_requests_lites`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zi_requests_pros`
--
ALTER TABLE `zi_requests_pros`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `zi_requests_servers`
--
ALTER TABLE `zi_requests_servers`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `zi_roles`
--
ALTER TABLE `zi_roles`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zi_users`
--
ALTER TABLE `zi_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `zi_user_tokens`
--
ALTER TABLE `zi_user_tokens`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `zi_roles_users`
--
ALTER TABLE `zi_roles_users`
ADD CONSTRAINT `zi_roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `zi_users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `zi_roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `zi_roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `zi_user_tokens`
--
ALTER TABLE `zi_user_tokens`
ADD CONSTRAINT `zi_user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `zi_users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
