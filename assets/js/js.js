$(document).ready(function(){

	//Video
	var player = $('#player');
	var idleTimer = null;
	var idleState = false;
	var idleWait = 3000; 

	$('#playBtn').click(function(){
		$('body').addClass('active');
		$('#nav-inner').fadeOut('fast');
		$('video').fadeIn('slow', function(){
			$('html, body').animate({ scrollTop: $('video').offset().top }, 250)
		});
		player.get(0).play();
		$('.preview').slideDown('fast', function(){
			$(this).animate({ height: $(window).height() + 'px' }, 600)
		});
		$('.preview .wrap').fadeOut('fast');
		$('#closeBtn').delay(150).fadeIn('slow', function(){
			$(this).animate({ left: -($('.bd').width() - $('#closeBtn').width())/2 + 'px' }, 300)
		});
		player.addClass('opened');
	});
	$('#closeBtn').click(function(){
		$('video').fadeOut('slow');
		$('#startBtn').delay(200).fadeIn('fast');
		$('#closeBtn').fadeOut('fast', function(){
			$(this).css('left','0')
		});
		player.get(0).pause();
		player.get(0).currentTime=0;
		$('.preview').css('height', '570px');
		$('.preview .wrap').fadeIn('fast');
		$('body').removeClass('active');
		$('#nav-inner').delay(200).fadeIn('fast');
		player.removeClass('opened');
		$('#navTop').slideDown('fast');
		clearTimeout(idleTimer);
	});
	/*player.hover(
		function(){
			$('#navTop').slideDown('fast');
			setTimeout(function(){
				$('#navTop').slideUp('fast');
			}, 3000);
		},function(){
			if($(event.relatedTarget).attr('id') !== 'navTop' && $(player).hasClass('opened'))
			{
				$('#navTop').slideUp('fast');
			}
		}
	);*/
	$('body').on('mousemove', '#player', function(){
		clearTimeout(idleTimer);
		if(idleState == true){ 
			$('#navTop').slideDown('fast');
		}
		idleState = false;
		idleTimer = setTimeout(function(){ 
			$('#navTop').slideUp('fast');
			idleState = true; 
		}, idleWait);
	});
	player.bind('ended', function(){
		$('#closeBtn').click()
	});
	
	//Table tariffs
	$('#compare').click(function(){
		$('.table-price').css('display')=='none' ? 
			$('.table-price').slideDown('fast', function(){
				$('html, body').animate({ scrollTop: $('.table-price').offset().top - 150 }, 600)
			}) : 
			$('.table-price').slideUp('fast', function(){
				$('html, body').animate({ scrollTop: $('.table-price').offset().top - $('.select-your').height() }, 800)
			});
	});

	//Call
	$('#call').click(function(e){
		$(this).addClass('active');
		$('.selectbox-call ul').addClass('open');
		$('.phone').focus();
		$('#arrow-pointer').addClass('rotate');
		e.stopPropagation();
	});
	$('.wrapper, #arrow-pointer').click(function(){
		$('#call').removeClass('active');
		$('.selectbox-call ul').removeClass('open');
		$('#arrow-pointer').removeClass('rotate');
	});
	$(document).bind('keyup', function(e){
		if(e.keyCode==27) {
			$('#call').removeClass('active');
			$('.selectbox-call ul').removeClass('open');
			$('#arrow-pointer').removeClass('rotate');
		}
	});

	//Select
	$('.input').click(function(e){
		$(this).toggleClass('active');
		$(this).parent().find('.selectbox').slideToggle(100);
		e.stopPropagation();
	});
	$(document).click(function(){
		$('.input').removeClass('active');
		$('.selectbox').slideUp(100);
	})

	// Phone
	$('#sendCallback').click(function() {
		var disableEl = $('[name=phone_call],#sendCallback');
		disableEl.attr('disabled', 'disabled');
		disableEl.css('opacity', 0.5);

		var $phone = $('[name=phone_call]');
		if ($phone.val() != '')
		{
			$.post(
				$('[name=form_callback]').prop('action'),
				{ 'phone': $phone.val() },
				function(res) {
					disableEl.removeAttr('disabled');
					disableEl.css('opacity', 1);
					if (res === 'OK')
					{
						$('.send-request').hide();
						$('.request').show();
					}
					else
					{
						$phone.addClass('error').focus();
						setTimeout(function(){
							$phone.removeClass('error')
						}, 1000);
					}
			});
		}
		else
		{
			disableEl.removeAttr('disabled');
			disableEl.css('opacity', 1);
			$phone.addClass('error').focus();
			setTimeout(function(){
				$phone.removeClass('error')
			}, 1000);
		}

		return false;
	});

	$('.phone').bind('change keyup input click', function() {
		// Type in input only numbers and '+' (less than 12 digits)
		var reg = /[^\+0-9]/;
		if (this.value.match(reg) || this.value.length > 12)
		{
			this.value = this.value.replace(reg, '');
			this.value = this.value.substr(0, 12);
		}
	});

	$('.num').bind('change keyup input click', function() {
		// Type in input only numbers (less than 3 digits)
		var reg = /[^0-9]/;
		if (this.value.match(reg) || this.value.length > 3)
		{
			this.value = this.value.replace(reg, '');
			this.value = this.value.substr(0, 3);
		}

		// Change price on event
		var $input = $(this);
		var vis = (this.value > 1 || (this.value != 0 && this.name !== 'num_set' && this.name !== 'num_employees')) ? 'visible' : 'hidden';
		$input.parent().find('.minus').css('visibility', vis);

		if (this.value == 0 && (this.name === 'num_set' || this.name === 'num_employees'))
		{
			this.value = 1;
		}

		$input.val(this.value);

		if (this.name === 'num_set')
		{
			setPrice($input.val(), $input.parents().find('#pricePro'), 'price_pro');
		}
		else if (this.name === 'num_big_card')
		{
			setPrice($input.val(), $input.parents().find('#priceBigCard'), 'price_big_card');
		}
		else if (this.name === 'num_flash_card')
		{
			setPrice($input.val(), $input.parents().find('#priceFlashCard'), 'price_flash_card');
		}
		else if (this.name === 'num_employees')
		{
			setPriceServer($input.val(), $input.parents().find('#priceServer'));
		}
	});

	/* Parallax */
	if ($('#main-page').length > 0)
	{
		$(window).scroll(function(){
			var player = $('#player');
			var awards = $('.awards');
			var excerpt = $('.excerpt');
			if (player.length > 0)
			{
				player.css({ 'background-position': 'center ' + ($(document).scrollTop() - player.offset().top) * 0.5 + 'px' });
			}
			if (awards.length > 0)
			{
				awards.css({ 'background-position': 'center ' + ($(document).scrollTop() - awards.offset().top) * 0.5 + 'px' });
			}
			if (excerpt.length > 0)
			{
				excerpt.css({ 'background-position': 'center ' + ($(document).scrollTop() - excerpt.offset().top) * 0.5 + 'px' });
			}
		});
	}

	//Zoom Pic Blog
	$('section .pic img').load(function(){
		changePic();
	});
	function changePic(){
		$('section .pic img').each(function(){
			var top = ($(this).height() - $(this).parent('section .pic').height())/2;
			$(this).css({
				'top': - top + 'px'
			})
		});
	};
	changePic();

	//Answer
	$('.answer li').click(function(){
		var active = $(this).hasClass('active');
		$('.answer li').removeClass('active');
		$('.answer li p').slideUp('fast');
		if (active)
		{
			$(this).children('.answer li p').slideUp('fast');
			$(this).removeClass('active');
		}
		else
		{
			$(this).children('.answer li p').slideDown('fast');
			$(this).addClass('active');
		}
	});

	//The number of sets
	$('.list-item .minus').css('visibility', 'hidden');
	$('#minusNumEmployees').css('visibility', 'visible');

	$('#minusNumPro').click(function(){
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		if (count == 1)
		{
			$(this).css('visibility', 'hidden');
		}
		$input.val(count);
		$input.change();

		// set new price
		setPrice($input.val(), $(this).parents().find('#pricePro'), 'price_pro');
	});
	$('#minusNumBigCard').click(function(){
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 0 ? 0 : count;
		if (count == 0)
		{
			$(this).css('visibility', 'hidden');
		}
		$input.val(count);
		$input.change();

		// set new price
		setPrice($input.val(), $(this).parents().find('#priceBigCard'), 'price_big_card');
	});
	$('#minusNumFlashCard').click(function(){
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 0 ? 0 : count;
		if (count == 0)
		{
			$(this).css('visibility', 'hidden');
		}
		$input.val(count);
		$input.change();

		// set new price
		setPrice($input.val(), $(this).parents().find('#priceFlashCard'), 'price_flash_card');
	});
	$('#minusNumEmployees').click(function(){
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		if (count == 1)
		{
			$(this).css('visibility', 'hidden');
		}
		$input.val(count);
		$input.change();

		// set new price server
		setPriceServer($input.val(), $(this).parents().find('#priceServer'));
	});
	$('#plusNumPro').click(function(){
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		$(this).parent().find('.minus').css('visibility', 'visible');

		// set new price
		setPrice($input.val(), $(this).parents().find('#pricePro'), 'price_pro');
	});
	$('#plusNumBigCard').click(function(){
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		$(this).parent().find('.minus').css('visibility', 'visible');

		// set new price
		setPrice($input.val(), $(this).parents().find('#priceBigCard'), 'price_big_card');
	});
	$('#plusNumFlashCard').click(function(){
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		$(this).parent().find('.minus').css('visibility', 'visible');

		// set new price
		setPrice($input.val(), $(this).parents().find('#priceFlashCard'), 'price_flash_card');
	});
	$('#plusNumEmployees').click(function(){
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		$(this).parent().find('.minus').css('visibility', 'visible');

		// set new price server
		setPriceServer($input.val(), $(this).parents().find('#priceServer'));
	});

	//Tooltip
	/*$('.pic-icon').hover(
		function(){
			$('.tooltip').fadeIn('fast');
		},
		function(){
			$('.tooltip').fadeOut('fast');
		}
	);
	$(window).resize(function(){
		$('.tooltip').css({
			'left': ($(document).width() - $('.tooltip').outerWidth())/2 + 'px'
		})
	});
	$(window).resize();*/

	$('[data-tooltip]').mousemove(function(e){
		$data_tooltip = $(this).attr('data-tooltip');
		var self = this;

		$('.tooltip').text($data_tooltip)
			.css({ 
				'left': $(self).offset().left + (($(self).find('.pic-icon').outerWidth() - $('.tooltip').outerWidth())/2) + 'px'
			})
			.show();
			console.log($('.tooltip').outerWidth());
		}).mouseout(function(){
			$('.tooltip').hide().text('')
	}).click(function(){
		return false;
	});

	//
	$('#submitPro').click(function() {
		var disableEl = $(this);
		disableEl.attr('disabled', 'disabled');
		disableEl.css('opacity', 0.5)

		$.post(
			$('[name=form_order_pro]').prop('action'),
			{
				'num_set': $('[name=num_set]').val(),
				'num_big_card': $('[name=num_big_card]').val(),
				'num_flash_card': $('[name=num_flash_card]').val(),
				'phone': $('[name=phone]').val(),
				'address': $('[name=address]').val(),
				'info': $('[name=info]').val(),
				'paid_type': $('[name=paid_type]:checked').val()
			},
			function(res) {
				disableEl.removeAttr('disabled');
				disableEl.css('opacity', 1);

				if (res)
				{
					res = $.parseJSON(res);
					if (res.sent == 1)
					{
						var siteName = $('#siteName').val();
						window.location.href = "http://" + siteName + "/download_accept";
					}
					else if (res.errors)
					{
						var form = $('[name=form_order_pro]');
						for(var n in res.errors)
						{
							$(form).find('input[name="'+n+'"]').addClass('error').focus();
						}
					}
				}
				else
				{
					alert('Заполните контактные данные');
				}
			}
		);

		return false;
	});

	$('#submitLite').click(function() {
		var disableEl = $(this);
		disableEl.attr('disabled', 'disabled');
		disableEl.css('opacity', 0.5);

		$.post(
			$('[name=form_order_lite]').prop('action'),
			{
				'period': $('[name=period]').val(),
				'email': $('[name=email]').val(),
				'zi_email': $('[name=zi_email]').val(),
				'paid_type': $('[name=paid_type]:checked').val()
			},
			function(res) {
				disableEl.removeAttr('disabled');
				disableEl.css('opacity', 1);

				if (res)
				{
					res = $.parseJSON(res);
					if (res.sent == 1)
					{
						var siteName = $('#siteName').val();
						window.location.href = "http://" + siteName + "/download_accept";
					}
					else if (res.errors)
					{
						var form = $('[name=form_order_lite]');
						for(var n in res.errors)
						{
							$(form).find('input[name="'+n+'"]').addClass('error').focus();
						}
					}
				}
				else
				{
					alert('Укажите email');
				}
			}
		);

		return false;
	});

	$('#submitServer').click(function() {
		var disableEl = $(this);
		disableEl.attr('disabled', 'disabled');
		disableEl.css('opacity', 0.5)

		$.post(
			$('[name=form_order_server]').prop('action'),
			{
				'num_employees': $('[name=num_employees]').val(),
				'name': $('[name=name]').val(),
				'phone': $('[name=phone]').val(),
				'email': $('[name=email]').val()
			},
			function(res) {
				disableEl.removeAttr('disabled');
				disableEl.css('opacity', 1);

				if (res)
				{
					res = $.parseJSON(res);
					if (res.sent == 1)
					{
						var siteName = $('#siteName').val();
						window.location.href = "http://" + siteName + "/download_accept";
					}
					else if (res.errors)
					{
						var form = $('[name=form_order_server]');
						for(var n in res.errors)
						{
							$(form).find('input[name="'+n+'"]').addClass('error').focus();
						}
					}
				}
				else
				{
					alert('Укажите контактные данные');
				}
			}
		);

		return false;
	});

	$('[id^=itemPeriod]').click(function() {
		var thisId = '#' + $(this).attr('id');
		var $this = $(this);
		var tempVal = $('#period1').attr('value');
		var tempText = $('#period1').text();

		// Set new period in hidden input
		$('[name=period]').val($(thisId).attr('value'));

		// Update labels with price and period
		$('#period1').attr('value', $this.attr('value'));
		$('#period1,#periodButton').text($this.text());
		$(thisId).attr('value', tempVal);
		$(thisId).text(tempText);

		// set new price lite
		setPriceLite($('[name=period]').val(), $('#priceLite'));
	});

	$('[name=email]').bind('keyup change input click', function() {
		$('#emailToSend').text($(this).val());
	});

	//Resize
	$(window).resize(function(){
		$('.preview .wrap').css({
			'left': ($(document).width() - $('.preview .wrap').width())/2 + 'px'
		});
		$('.awards .wrap').css({
			'top': ($('.awards').height() - $('.awards .wrap').outerHeight())/2 - 110 + 'px',
			'left': ($('.awards').width() - $('.awards .wrap').width())/2 + 'px'
		});
		$('.excerpt h3').css({
			'position':'relative',
			'top': ($('.excerpt').height() - $('.excerpt h3').height())/2 + 'px'
		});
	});
	$(window).resize();

	// Set new price
	// int		cnt	number of sets|cards
	// object	label	price value object
	// string	name	hidden input name
	function setPrice(cnt, label, name)
	{
		var newPrice = cnt * $('[name=' + name + ']').val();
		label.html(number_format(newPrice, 0, ',', ' ') + '$');

		// Calculate new total price
		var total = $('[name=price_pro]').val() * $('[name=num_set]').val()
			+ $('[name=price_big_card]').val() * $('[name=num_big_card]').val()
			+ $('[name=price_flash_card]').val() * $('[name=num_flash_card]').val();
		var price = number_format(total, 0, ',', ' ') + '$';
		$('#totalPrice,#totalPriceButton').html(price);
		$('[name=total]').val(total);
	}

	function setPriceLite(cnt, label)
	{
		var newPrice = cnt * $('[name=price_lite]').val();
		label.html(number_format(newPrice, 0, ',', ' ') + '$');

		// Calculate new total price
		var total = $('[name=price_lite]').val() * $('[name=period]').val();
		var price = number_format(total, 0, ',', ' ') + '$';
		$('#totalPriceLiteButton').html(price);
		$('[name=total]').val(total);
	}

	function setPriceServer(cnt, label)
	{
		var newPrice = cnt * $('[name=price_server]').val();
		label.html(number_format(newPrice, 0, ',', ' ') + '$');

		// Calculate new total price
		var total = $('[name=price_server]').val() * $('[name=num_employees]').val();
		var price = number_format(total, 0, ',', ' ') + '$';
		$('#totalPriceServerButton').html(price);
		$('[name=total]').val(total);
	}
});

//  discuss at: http://phpjs.org/functions/number_format/
function number_format(number, decimals, dec_point, thousands_sep)
{
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function(n, prec) {
			var k = Math.pow(10, prec);
			return '' + (Math.round(n * k) / k).toFixed(prec);
		};

	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3)
	{
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec)
	{
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}
